from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal.StructBase import StructBase
from .......Internal.ArgStruct import ArgStruct


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class Le1MCls:
	"""Le1M commands group definition. 3 total commands, 0 Subgroups, 3 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("le1M", core, parent)

	# noinspection PyTypeChecker
	class ResultData(StructBase):
		"""Response structure. Fields: \n
			- Reliability: int: No parameter help available
			- Per: float: No parameter help available
			- Packets_Received: int: No parameter help available
			- Search_Result: float: No parameter help available"""
		__meta_args_list = [
			ArgStruct.scalar_int('Reliability', 'Reliability'),
			ArgStruct.scalar_float('Per'),
			ArgStruct.scalar_int('Packets_Received'),
			ArgStruct.scalar_float('Search_Result')]

		def __init__(self):
			StructBase.__init__(self, self)
			self.Reliability: int = None
			self.Per: float = None
			self.Packets_Received: int = None
			self.Search_Result: float = None

	def read(self) -> ResultData:
		"""SCPI: READ:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE1M \n
		Snippet: value: ResultData = driver.dtMode.rxQuality.search.per.lowEnergy.le1M.read() \n
		No command help available \n
			:return: structure: for return value, see the help for ResultData structure arguments."""
		return self._core.io.query_struct(f'READ:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE1M?', self.__class__.ResultData())

	def fetch(self) -> ResultData:
		"""SCPI: FETCh:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE1M \n
		Snippet: value: ResultData = driver.dtMode.rxQuality.search.per.lowEnergy.le1M.fetch() \n
		No command help available \n
			:return: structure: for return value, see the help for ResultData structure arguments."""
		return self._core.io.query_struct(f'FETCh:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE1M?', self.__class__.ResultData())

	# noinspection PyTypeChecker
	class CalculateStruct(StructBase):
		"""Response structure. Fields: \n
			- Reliability: int: No parameter help available
			- Per: float or bool: No parameter help available
			- Packets_Received: float or bool: No parameter help available
			- Search_Result: float or bool: No parameter help available"""
		__meta_args_list = [
			ArgStruct.scalar_int('Reliability', 'Reliability'),
			ArgStruct.scalar_float_ext('Per'),
			ArgStruct.scalar_float_ext('Packets_Received'),
			ArgStruct.scalar_float_ext('Search_Result')]

		def __init__(self):
			StructBase.__init__(self, self)
			self.Reliability: int = None
			self.Per: float or bool = None
			self.Packets_Received: float or bool = None
			self.Search_Result: float or bool = None

	def calculate(self) -> CalculateStruct:
		"""SCPI: CALCulate:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE1M \n
		Snippet: value: CalculateStruct = driver.dtMode.rxQuality.search.per.lowEnergy.le1M.calculate() \n
		No command help available \n
			:return: structure: for return value, see the help for CalculateStruct structure arguments."""
		return self._core.io.query_struct(f'CALCulate:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE1M?', self.__class__.CalculateStruct())
