DtMode
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Call.DtMode.DtModeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.dtMode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_DtMode_LowEnergy.rst