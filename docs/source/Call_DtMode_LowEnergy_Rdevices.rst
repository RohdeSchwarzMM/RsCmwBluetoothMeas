Rdevices
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALL:BLUetooth:MEASurement<Instance>:DTMode:LENergy:RDEVices

.. code-block:: python

	CALL:BLUetooth:MEASurement<Instance>:DTMode:LENergy:RDEVices



.. autoclass:: RsCmwBluetoothMeas.Implementations.Call.DtMode.LowEnergy.Rdevices.RdevicesCls
	:members:
	:undoc-members:
	:noindex: