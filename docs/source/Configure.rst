Configure
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HWINterface
	single: CONFigure:BLUetooth:MEASurement<Instance>:CPRotocol
	single: CONFigure:BLUetooth:MEASurement<Instance>:GDELay
	single: CONFigure:BLUetooth:MEASurement<Instance>:CFILter
	single: CONFigure:BLUetooth:MEASurement<Instance>:OTHReshold

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HWINterface
	CONFigure:BLUetooth:MEASurement<Instance>:CPRotocol
	CONFigure:BLUetooth:MEASurement<Instance>:GDELay
	CONFigure:BLUetooth:MEASurement<Instance>:CFILter
	CONFigure:BLUetooth:MEASurement<Instance>:OTHReshold



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_ComSettings.rst
	Configure_Display.rst
	Configure_DtMode.rst
	Configure_Hdr.rst
	Configure_Hdrp.rst
	Configure_InputSignal.rst
	Configure_MultiEval.rst
	Configure_RfSettings.rst
	Configure_RxQuality.rst
	Configure_Trx.rst