ComSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:COMPort
	single: CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:BAUDrate
	single: CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:STOPbits
	single: CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:PARity
	single: CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:PROTocol
	single: CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:DBITs

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:COMPort
	CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:BAUDrate
	CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:STOPbits
	CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:PARity
	CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:PROTocol
	CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:DBITs



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.ComSettings.ComSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.comSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_ComSettings_Ports.rst