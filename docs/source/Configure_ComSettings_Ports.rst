Ports
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:PORTs:CATalog

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:COMSettings:PORTs:CATalog



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.ComSettings.Ports.PortsCls
	:members:
	:undoc-members:
	:noindex: