Display
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:DISPlay

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:DISPlay



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Display.DisplayCls
	:members:
	:undoc-members:
	:noindex: