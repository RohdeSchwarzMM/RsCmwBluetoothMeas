DtMode
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.DtMode.DtModeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.dtMode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_DtMode_RxQuality.rst