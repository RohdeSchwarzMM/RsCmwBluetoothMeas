Eattenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:EATTenuation:OUTPut

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:EATTenuation:OUTPut



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.DtMode.RxQuality.Eattenuation.EattenuationCls
	:members:
	:undoc-members:
	:noindex: