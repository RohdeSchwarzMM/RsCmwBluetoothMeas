Mper
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.DtMode.RxQuality.Search.Limit.Mper.MperCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.dtMode.rxQuality.search.limit.mper.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_DtMode_RxQuality_Search_Limit_Mper_LowEnergy.rst