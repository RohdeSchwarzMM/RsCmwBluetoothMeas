LowEnergy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:LIMit:MPER:LENergy:LE1M
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:LIMit:MPER:LENergy:LE2M
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:LIMit:MPER:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:LIMit:MPER:LENergy:LE1M
	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:LIMit:MPER:LENergy:LE2M
	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:LIMit:MPER:LENergy:LRANge



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.DtMode.RxQuality.Search.Limit.Mper.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex: