Packets
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.DtMode.RxQuality.Search.Packets.PacketsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.dtMode.rxQuality.search.packets.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_DtMode_RxQuality_Search_Packets_LowEnergy.rst