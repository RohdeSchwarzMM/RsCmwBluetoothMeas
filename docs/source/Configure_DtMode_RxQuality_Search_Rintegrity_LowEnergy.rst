LowEnergy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:RINTegrity:LENergy:LE1M
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:RINTegrity:LENergy:LE2M
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:RINTegrity:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:RINTegrity:LENergy:LE1M
	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:RINTegrity:LENergy:LE2M
	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:RINTegrity:LENergy:LRANge



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.DtMode.RxQuality.Search.Rintegrity.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex: