InputSignal
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:PATTern
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:PLENgth
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:PTYPe
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:NAP
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:UAP
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:LAP
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:BDADdress
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:ASYNchronize
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:DMODe

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:PATTern
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:PLENgth
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:PTYPe
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:NAP
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:UAP
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:LAP
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:BDADdress
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:ASYNchronize
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:DMODe



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdr.InputSignal.InputSignalCls
	:members:
	:undoc-members:
	:noindex: