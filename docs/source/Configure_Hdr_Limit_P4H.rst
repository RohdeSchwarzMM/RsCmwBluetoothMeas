P4H
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:LIMit:P4H:DEVM
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:LIMit:P4H:SGACp

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDR:LIMit:P4H:DEVM
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:LIMit:P4H:SGACp



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdr.Limit.P4H.P4HCls
	:members:
	:undoc-members:
	:noindex: