P8H
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:LIMit:P8H:DEVM
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:LIMit:P8H:SGACp

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDR:LIMit:P8H:DEVM
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:LIMit:P8H:SGACp



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdr.Limit.P8H.P8HCls
	:members:
	:undoc-members:
	:noindex: