Pencoding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:LIMit:PENCoding

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDR:LIMit:PENCoding



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdr.Limit.Pencoding.PencodingCls
	:members:
	:undoc-members:
	:noindex: