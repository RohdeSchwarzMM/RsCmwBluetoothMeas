Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult[:ALL]
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:PVTime
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:SGACp
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:PENCoding
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:IQERr
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:IQDiff
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:IQABsolute
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:PDIFference
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:DEVMagnitude
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:TXSCalar

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult[:ALL]
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:PVTime
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:SGACp
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:PENCoding
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:IQERr
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:IQDiff
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:IQABsolute
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:PDIFference
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:DEVMagnitude
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:RESult:TXSCalar



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdr.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: