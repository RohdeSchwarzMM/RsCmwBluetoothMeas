Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:SCOunt:PVTime
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:SCOunt:SGACp
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:SCOunt:MODulation
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:SCOunt:PENCoding

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDR:SCOunt:PVTime
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:SCOunt:SGACp
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:SCOunt:MODulation
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:SCOunt:PENCoding



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdr.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: