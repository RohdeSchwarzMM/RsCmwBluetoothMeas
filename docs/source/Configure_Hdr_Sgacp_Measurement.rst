Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:SGACp:MEASurement:MODE

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDR:SGACp:MEASurement:MODE



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdr.Sgacp.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: