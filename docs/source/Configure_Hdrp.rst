Hdrp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:MOEXception
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:SCONdition
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:REPetition

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:MOEXception
	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:SCONdition
	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:REPetition



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdrp.HdrpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.hdrp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Hdrp_InputSignal.rst
	Configure_Hdrp_Limit.rst
	Configure_Hdrp_Result.rst
	Configure_Hdrp_Sacp.rst
	Configure_Hdrp_Scount.rst