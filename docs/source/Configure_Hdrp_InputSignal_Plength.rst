Plength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:PLENgth

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:PLENgth



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdrp.InputSignal.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex: