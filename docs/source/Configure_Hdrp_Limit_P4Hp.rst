P4Hp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:LIMit:P4HP:SACP

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:LIMit:P4HP:SACP



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdrp.Limit.P4Hp.P4HpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.hdrp.limit.p4Hp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Hdrp_Limit_P4Hp_EvMagnitude.rst