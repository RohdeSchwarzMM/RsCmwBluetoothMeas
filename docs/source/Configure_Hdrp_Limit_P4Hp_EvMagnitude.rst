EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:LIMit:P4HP:EVMagnitude:OFFSet

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:LIMit:P4HP:EVMagnitude:OFFSet



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdrp.Limit.P4Hp.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: