P8Hp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:LIMit:P8HP:SACP

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:LIMit:P8HP:SACP



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdrp.Limit.P8Hp.P8HpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.hdrp.limit.p8Hp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Hdrp_Limit_P8Hp_EvMagnitude.rst