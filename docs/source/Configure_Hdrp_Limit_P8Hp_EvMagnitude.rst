EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:LIMit:P8HP:EVMagnitude:OFFSet

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:LIMit:P8HP:EVMagnitude:OFFSet



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdrp.Limit.P8Hp.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: