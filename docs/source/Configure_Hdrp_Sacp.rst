Sacp
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdrp.Sacp.SacpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.hdrp.sacp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Hdrp_Sacp_Measurement.rst