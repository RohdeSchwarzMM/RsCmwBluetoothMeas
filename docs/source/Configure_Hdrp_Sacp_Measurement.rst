Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:SACP:MEASurement:MODE

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:SACP:MEASurement:MODE



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Hdrp.Sacp.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: