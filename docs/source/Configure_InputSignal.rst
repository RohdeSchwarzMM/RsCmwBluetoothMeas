InputSignal
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:DMODe
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:BTYPe
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:ASYNchronize

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:DMODe
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:BTYPe
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:ASYNchronize



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.InputSignalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.inputSignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_InputSignal_Aacc.rst
	Configure_InputSignal_AccAddress.rst
	Configure_InputSignal_BdAddress.rst
	Configure_InputSignal_Cscheme.rst
	Configure_InputSignal_Cte.rst
	Configure_InputSignal_Dacc.rst
	Configure_InputSignal_DtMode.rst
	Configure_InputSignal_Fec.rst
	Configure_InputSignal_Lap.rst
	Configure_InputSignal_LowEnergy.rst
	Configure_InputSignal_Nap.rst
	Configure_InputSignal_Oslots.rst
	Configure_InputSignal_Pattern.rst
	Configure_InputSignal_Plength.rst
	Configure_InputSignal_Ptype.rst
	Configure_InputSignal_Qhsl.rst
	Configure_InputSignal_SynWord.rst
	Configure_InputSignal_Uap.rst