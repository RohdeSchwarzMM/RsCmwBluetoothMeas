LowEnergy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CSCHeme:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CSCHeme:LENergy:LRANge



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Cscheme.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex: