LowEnergy
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Cte.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.inputSignal.cte.lowEnergy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_InputSignal_Cte_LowEnergy_Le1M.rst
	Configure_InputSignal_Cte_LowEnergy_Le2M.rst