Le1M
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:LENergy:LE1M:TYPE
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:LENergy:LE1M:UNITs

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:LENergy:LE1M:TYPE
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:LENergy:LE1M:UNITs



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Cte.LowEnergy.Le1M.Le1MCls
	:members:
	:undoc-members:
	:noindex: