Le2M
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:LENergy:LE2M:TYPE
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:LENergy:LE2M:UNITs

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:LENergy:LE2M:TYPE
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:LENergy:LE2M:UNITs



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Cte.LowEnergy.Le2M.Le2MCls
	:members:
	:undoc-members:
	:noindex: