P2Q
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P2Q:TYPE
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P2Q:UNITs

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P2Q:TYPE
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P2Q:UNITs



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Cte.Qhsl.P2Q.P2QCls
	:members:
	:undoc-members:
	:noindex: