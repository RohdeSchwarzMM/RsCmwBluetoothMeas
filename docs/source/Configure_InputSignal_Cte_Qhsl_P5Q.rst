P5Q
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P5Q:TYPE
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P5Q:UNITs

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P5Q:TYPE
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P5Q:UNITs



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Cte.Qhsl.P5Q.P5QCls
	:members:
	:undoc-members:
	:noindex: