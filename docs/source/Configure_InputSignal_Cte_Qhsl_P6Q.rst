P6Q
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P6Q:TYPE
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P6Q:UNITs

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P6Q:TYPE
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P6Q:UNITs



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Cte.Qhsl.P6Q.P6QCls
	:members:
	:undoc-members:
	:noindex: