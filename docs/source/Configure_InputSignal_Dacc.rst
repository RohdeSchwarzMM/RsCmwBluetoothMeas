Dacc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:DACC:QHSL

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:DACC:QHSL



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Dacc.DaccCls
	:members:
	:undoc-members:
	:noindex: