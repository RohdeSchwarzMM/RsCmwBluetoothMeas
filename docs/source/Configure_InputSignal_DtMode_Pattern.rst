Pattern
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.DtMode.Pattern.PatternCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.inputSignal.dtMode.pattern.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_InputSignal_DtMode_Pattern_LowEnergy.rst