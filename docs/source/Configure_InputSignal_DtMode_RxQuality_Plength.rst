Plength
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.DtMode.RxQuality.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.inputSignal.dtMode.rxQuality.plength.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_InputSignal_DtMode_RxQuality_Plength_LowEnergy.rst