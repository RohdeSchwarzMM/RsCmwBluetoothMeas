Fec
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Fec.FecCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.inputSignal.fec.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_InputSignal_Fec_LowEnergy.rst