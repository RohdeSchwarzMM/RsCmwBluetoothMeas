LowEnergy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:FEC:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:FEC:LENergy:LRANge



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Fec.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex: