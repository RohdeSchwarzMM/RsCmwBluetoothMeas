Lap
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LAP:QHSL
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LAP

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LAP:QHSL
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LAP



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Lap.LapCls
	:members:
	:undoc-members:
	:noindex: