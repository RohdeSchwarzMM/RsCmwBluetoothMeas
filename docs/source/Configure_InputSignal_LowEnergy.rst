LowEnergy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LENergy:SYNWord
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LENergy:PHY

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LENergy:SYNWord
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LENergy:PHY



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex: