Oslots
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:OSLots:EDRate
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:OSLots:BRATe

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:OSLots:EDRate
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:OSLots:BRATe



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Oslots.OslotsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.inputSignal.oslots.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_InputSignal_Oslots_LowEnergy.rst