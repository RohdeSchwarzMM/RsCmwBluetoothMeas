Plength
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:BRATe
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:EDRate

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:BRATe
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:EDRate



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.inputSignal.plength.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_InputSignal_Plength_LowEnergy.rst
	Configure_InputSignal_Plength_Qhsl.rst