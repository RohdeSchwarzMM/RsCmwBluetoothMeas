Qhsl
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:QHSL:P2Q
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:QHSL:P3Q
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:QHSL:P4Q
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:QHSL:P5Q
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:QHSL:P6Q

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:QHSL:P2Q
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:QHSL:P3Q
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:QHSL:P4Q
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:QHSL:P5Q
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:QHSL:P6Q



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Plength.Qhsl.QhslCls
	:members:
	:undoc-members:
	:noindex: