Ptype
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:EDRate
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:BRATe

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:EDRate
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:BRATe



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Ptype.PtypeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.inputSignal.ptype.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_InputSignal_Ptype_LowEnergy.rst
	Configure_InputSignal_Ptype_Qhsl.rst