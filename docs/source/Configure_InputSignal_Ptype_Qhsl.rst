Qhsl
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:QHSL:P2Q
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:QHSL:P3Q
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:QHSL:P4Q
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:QHSL:P5Q
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:QHSL:P6Q

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:QHSL:P2Q
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:QHSL:P3Q
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:QHSL:P4Q
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:QHSL:P5Q
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:QHSL:P6Q



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Ptype.Qhsl.QhslCls
	:members:
	:undoc-members:
	:noindex: