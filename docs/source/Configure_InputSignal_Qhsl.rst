Qhsl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:QHSL:PHY

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:QHSL:PHY



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.InputSignal.Qhsl.QhslCls
	:members:
	:undoc-members:
	:noindex: