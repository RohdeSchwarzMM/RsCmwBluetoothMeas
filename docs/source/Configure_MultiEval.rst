MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:REPetition

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:REPetition



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Brate.rst
	Configure_MultiEval_Edrate.rst
	Configure_MultiEval_Frange.rst
	Configure_MultiEval_Limit.rst
	Configure_MultiEval_ListPy.rst
	Configure_MultiEval_LowEnergy.rst
	Configure_MultiEval_Malgorithm.rst
	Configure_MultiEval_Measurement.rst
	Configure_MultiEval_Qhsl.rst
	Configure_MultiEval_Result.rst
	Configure_MultiEval_Sacp.rst
	Configure_MultiEval_Scount.rst
	Configure_MultiEval_Sgacp.rst
	Configure_MultiEval_Synchronise.rst