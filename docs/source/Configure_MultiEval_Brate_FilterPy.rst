FilterPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:BRATe:FILTer:BWIDth

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:BRATe:FILTer:BWIDth



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Brate.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: