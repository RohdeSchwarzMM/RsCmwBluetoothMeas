Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:FRANge:BRATe:MEASurement

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:FRANge:BRATe:MEASurement



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Frange.Brate.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: