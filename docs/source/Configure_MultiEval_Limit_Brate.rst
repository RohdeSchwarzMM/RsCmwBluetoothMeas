Brate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:DAVerage
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:DMINimum
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:DMAXimum

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:DAVerage
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:DMINimum
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:DMAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Brate.BrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.brate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Brate_Delta.rst
	Configure_MultiEval_Limit_Brate_Faccuracy.rst
	Configure_MultiEval_Limit_Brate_Fdrift.rst
	Configure_MultiEval_Limit_Brate_Mratio.rst
	Configure_MultiEval_Limit_Brate_PowerVsTime.rst