Delta
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:DELTa

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:DELTa



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Brate.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex: