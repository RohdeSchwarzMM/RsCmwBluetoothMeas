Faccuracy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:FACCuracy

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:FACCuracy



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Brate.Faccuracy.FaccuracyCls
	:members:
	:undoc-members:
	:noindex: