Mratio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:MRATio

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:MRATio



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Brate.Mratio.MratioCls
	:members:
	:undoc-members:
	:noindex: