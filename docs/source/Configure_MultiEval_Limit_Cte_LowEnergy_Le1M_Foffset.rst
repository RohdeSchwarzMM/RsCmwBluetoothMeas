Foffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:CTE:LENergy:LE1M:FOFFset

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:CTE:LENergy:LE1M:FOFFset



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Cte.LowEnergy.Le1M.Foffset.FoffsetCls
	:members:
	:undoc-members:
	:noindex: