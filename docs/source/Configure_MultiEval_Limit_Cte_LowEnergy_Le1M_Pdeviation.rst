Pdeviation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:CTE:LENergy:LE1M:PDEViation

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:CTE:LENergy:LE1M:PDEViation



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Cte.LowEnergy.Le1M.Pdeviation.PdeviationCls
	:members:
	:undoc-members:
	:noindex: