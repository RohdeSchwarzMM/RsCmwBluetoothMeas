Pdeviation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:CTE:LENergy:LE2M:PDEViation

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:CTE:LENergy:LE2M:PDEViation



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Cte.LowEnergy.Le2M.Pdeviation.PdeviationCls
	:members:
	:undoc-members:
	:noindex: