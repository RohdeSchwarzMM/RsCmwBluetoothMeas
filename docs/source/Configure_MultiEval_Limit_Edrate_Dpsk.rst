Dpsk
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:EDRate:DPSK:DEVM

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:EDRate:DPSK:DEVM



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Edrate.Dpsk.DpskCls
	:members:
	:undoc-members:
	:noindex: