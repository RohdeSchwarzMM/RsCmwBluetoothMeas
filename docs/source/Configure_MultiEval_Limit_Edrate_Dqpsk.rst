Dqpsk
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:EDRate:DQPSk:DEVM

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:EDRate:DQPSk:DEVM



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Edrate.Dqpsk.DqpskCls
	:members:
	:undoc-members:
	:noindex: