Ssequence
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:EDRate:PENCoding:SSEQuence

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:EDRate:PENCoding:SSEQuence



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Edrate.Pencoding.Ssequence.SsequenceCls
	:members:
	:undoc-members:
	:noindex: