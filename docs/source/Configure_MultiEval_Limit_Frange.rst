Frange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:FRANge

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:FRANge



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Frange.FrangeCls
	:members:
	:undoc-members:
	:noindex: