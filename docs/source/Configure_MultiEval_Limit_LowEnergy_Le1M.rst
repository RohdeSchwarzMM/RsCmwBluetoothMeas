Le1M
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy[:LE1M]:FDRift

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy[:LE1M]:FDRift



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.LowEnergy.Le1M.Le1MCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.lowEnergy.le1M.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_LowEnergy_Le1M_Faccuracy.rst
	Configure_MultiEval_Limit_LowEnergy_Le1M_Foffset.rst
	Configure_MultiEval_Limit_LowEnergy_Le1M_Mratio.rst
	Configure_MultiEval_Limit_LowEnergy_Le1M_PowerVsTime.rst
	Configure_MultiEval_Limit_LowEnergy_Le1M_Sacp.rst