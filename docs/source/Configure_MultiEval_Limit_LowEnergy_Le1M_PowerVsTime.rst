PowerVsTime
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy[:LE1M]:PVTime

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy[:LE1M]:PVTime



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.LowEnergy.Le1M.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex: