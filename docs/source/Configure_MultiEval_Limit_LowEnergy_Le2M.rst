Le2M
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy:LE2M:FDRift

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy:LE2M:FDRift



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.LowEnergy.Le2M.Le2MCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.lowEnergy.le2M.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_LowEnergy_Le2M_Daverage.rst
	Configure_MultiEval_Limit_LowEnergy_Le2M_Delta.rst
	Configure_MultiEval_Limit_LowEnergy_Le2M_Dmaximum.rst
	Configure_MultiEval_Limit_LowEnergy_Le2M_Dminimum.rst
	Configure_MultiEval_Limit_LowEnergy_Le2M_Faccuracy.rst
	Configure_MultiEval_Limit_LowEnergy_Le2M_Foffset.rst
	Configure_MultiEval_Limit_LowEnergy_Le2M_Mratio.rst
	Configure_MultiEval_Limit_LowEnergy_Le2M_PowerVsTime.rst
	Configure_MultiEval_Limit_LowEnergy_Le2M_Sacp.rst