Daverage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy:LE2M:DAVerage

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy:LE2M:DAVerage



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.LowEnergy.Le2M.Daverage.DaverageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.lowEnergy.le2M.daverage.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_LowEnergy_Le2M_Daverage_Df2S.rst