PowerVsTime
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy:LE2M:PVTime

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy:LE2M:PVTime



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.LowEnergy.Le2M.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex: