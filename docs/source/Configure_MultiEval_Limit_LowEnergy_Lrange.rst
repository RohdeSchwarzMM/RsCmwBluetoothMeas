Lrange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy:LRANge:FDRift

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy:LRANge:FDRift



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.LowEnergy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.lowEnergy.lrange.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_LowEnergy_Lrange_Daverage.rst
	Configure_MultiEval_Limit_LowEnergy_Lrange_Delta.rst
	Configure_MultiEval_Limit_LowEnergy_Lrange_Dmaximum.rst
	Configure_MultiEval_Limit_LowEnergy_Lrange_Dminimum.rst
	Configure_MultiEval_Limit_LowEnergy_Lrange_Faccuracy.rst
	Configure_MultiEval_Limit_LowEnergy_Lrange_Foffset.rst
	Configure_MultiEval_Limit_LowEnergy_Lrange_PowerVsTime.rst
	Configure_MultiEval_Limit_LowEnergy_Lrange_Sacp.rst