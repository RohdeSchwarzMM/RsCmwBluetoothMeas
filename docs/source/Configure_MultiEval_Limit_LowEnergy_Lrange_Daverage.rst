Daverage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy:LRANge:DAVerage

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy:LRANge:DAVerage



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.LowEnergy.Lrange.Daverage.DaverageCls
	:members:
	:undoc-members:
	:noindex: