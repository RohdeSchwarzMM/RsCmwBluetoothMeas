Dminimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy:LRANge:DMINimum

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:LENergy:LRANge:DMINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.LowEnergy.Lrange.Dminimum.DminimumCls
	:members:
	:undoc-members:
	:noindex: