P3Q
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:QHSL:P3Q:DEVM

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:QHSL:P3Q:DEVM



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Qhsl.P3Q.P3QCls
	:members:
	:undoc-members:
	:noindex: