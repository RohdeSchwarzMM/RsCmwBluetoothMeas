P5Q
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:QHSL:P5Q:DEVM

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:QHSL:P5Q:DEVM



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Qhsl.P5Q.P5QCls
	:members:
	:undoc-members:
	:noindex: