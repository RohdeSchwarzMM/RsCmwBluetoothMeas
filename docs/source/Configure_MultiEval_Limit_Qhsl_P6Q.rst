P6Q
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:QHSL:P6Q:DEVM

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:QHSL:P6Q:DEVM



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Qhsl.P6Q.P6QCls
	:members:
	:undoc-members:
	:noindex: