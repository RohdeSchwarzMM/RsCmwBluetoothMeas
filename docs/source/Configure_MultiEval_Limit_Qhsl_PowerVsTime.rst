PowerVsTime
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:QHSL:PVTime

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:QHSL:PVTime



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Qhsl.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex: