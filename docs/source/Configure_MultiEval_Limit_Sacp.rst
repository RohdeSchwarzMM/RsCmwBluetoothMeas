Sacp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:SACP

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:SACP



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.Sacp.SacpCls
	:members:
	:undoc-members:
	:noindex: