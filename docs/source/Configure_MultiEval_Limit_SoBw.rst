SoBw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:SOBW

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:SOBW



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Limit.SoBw.SoBwCls
	:members:
	:undoc-members:
	:noindex: