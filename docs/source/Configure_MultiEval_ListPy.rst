ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:NCONnections
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:COUNt
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:MALGorithm
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:CMODe
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:NCONnections
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:COUNt
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:MALGorithm
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:CMODe
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_Segment.rst
	Configure_MultiEval_ListPy_SingleCmw.rst