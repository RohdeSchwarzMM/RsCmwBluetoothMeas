Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S128
	rc = driver.configure.multiEval.listPy.segment.repcap_segment_get()
	driver.configure.multiEval.listPy.segment.repcap_segment_set(repcap.Segment.S1)





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_Segment_Cidx.rst
	Configure_MultiEval_ListPy_Segment_Results.rst
	Configure_MultiEval_ListPy_Segment_Scount.rst
	Configure_MultiEval_ListPy_Segment_Setup.rst