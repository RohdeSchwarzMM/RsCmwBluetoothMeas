Results
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:RESults

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:RESults



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.ListPy.Segment.Results.ResultsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.segment.results.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_Segment_Results_Mscalar.rst
	Configure_MultiEval_ListPy_Segment_Results_Pencoding.rst
	Configure_MultiEval_ListPy_Segment_Results_Pscalar.rst
	Configure_MultiEval_ListPy_Segment_Results_Sacp.rst
	Configure_MultiEval_ListPy_Segment_Results_Sgacp.rst
	Configure_MultiEval_ListPy_Segment_Results_SoBw.rst