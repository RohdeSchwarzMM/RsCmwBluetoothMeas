Sacp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:RESults:SACP

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:RESults:SACP



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.ListPy.Segment.Results.Sacp.SacpCls
	:members:
	:undoc-members:
	:noindex: