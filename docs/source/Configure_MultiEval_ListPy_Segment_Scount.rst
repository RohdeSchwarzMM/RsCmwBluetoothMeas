Scount
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SCOunt

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SCOunt



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.ListPy.Segment.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.segment.scount.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_Segment_Scount_Mscalar.rst
	Configure_MultiEval_ListPy_Segment_Scount_Pencoding.rst
	Configure_MultiEval_ListPy_Segment_Scount_Pscalar.rst
	Configure_MultiEval_ListPy_Segment_Scount_Sacp.rst
	Configure_MultiEval_ListPy_Segment_Scount_Sgacp.rst
	Configure_MultiEval_ListPy_Segment_Scount_SoBw.rst