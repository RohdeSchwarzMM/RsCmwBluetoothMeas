Setup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:SETup]

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:SETup]



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.ListPy.Segment.Setup.SetupCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.segment.setup.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_Segment_Setup_Btype.rst
	Configure_MultiEval_ListPy_Segment_Setup_Cscheme.rst
	Configure_MultiEval_ListPy_Segment_Setup_Cte.rst
	Configure_MultiEval_ListPy_Segment_Setup_EnvelopePower.rst
	Configure_MultiEval_ListPy_Segment_Setup_Extended.rst
	Configure_MultiEval_ListPy_Segment_Setup_FilterPy.rst
	Configure_MultiEval_ListPy_Segment_Setup_Frequency.rst
	Configure_MultiEval_ListPy_Segment_Setup_MoException.rst
	Configure_MultiEval_ListPy_Segment_Setup_Oslots.rst
	Configure_MultiEval_ListPy_Segment_Setup_Pattern.rst
	Configure_MultiEval_ListPy_Segment_Setup_Phy.rst
	Configure_MultiEval_ListPy_Segment_Setup_Plength.rst
	Configure_MultiEval_ListPy_Segment_Setup_Ptype.rst
	Configure_MultiEval_ListPy_Segment_Setup_Qhsl.rst
	Configure_MultiEval_ListPy_Segment_Setup_Rtrigger.rst
	Configure_MultiEval_ListPy_Segment_Setup_SingleCmw.rst
	Configure_MultiEval_ListPy_Segment_Setup_Slength.rst