Btype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:SETup]:BTYPe

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:SETup]:BTYPe



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.ListPy.Segment.Setup.Btype.BtypeCls
	:members:
	:undoc-members:
	:noindex: