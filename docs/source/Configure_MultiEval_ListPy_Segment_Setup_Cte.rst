Cte
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.ListPy.Segment.Setup.Cte.CteCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.segment.setup.cte.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_Segment_Setup_Cte_TypePy.rst
	Configure_MultiEval_ListPy_Segment_Setup_Cte_Units.rst