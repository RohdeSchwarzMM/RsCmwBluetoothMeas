Extended
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:SETup]:EXTended

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:SETup]:EXTended



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.ListPy.Segment.Setup.Extended.ExtendedCls
	:members:
	:undoc-members:
	:noindex: