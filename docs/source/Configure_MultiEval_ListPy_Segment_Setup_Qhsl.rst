Qhsl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:SETup]:QHSL

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:SETup]:QHSL



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.ListPy.Segment.Setup.Qhsl.QhslCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.segment.setup.qhsl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_Segment_Setup_Qhsl_Phy.rst