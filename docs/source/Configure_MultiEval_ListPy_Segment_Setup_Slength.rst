Slength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:SETup]:SLENgth

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:SETup]:SLENgth



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.ListPy.Segment.Setup.Slength.SlengthCls
	:members:
	:undoc-members:
	:noindex: