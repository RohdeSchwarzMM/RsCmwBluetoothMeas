SingleCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<instance>:MEValuation:LIST:CMWS:CMODe

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<instance>:MEValuation:LIST:CMWS:CMODe



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.ListPy.SingleCmw.SingleCmwCls
	:members:
	:undoc-members:
	:noindex: