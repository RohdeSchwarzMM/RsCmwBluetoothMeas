LowEnergy
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.lowEnergy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_LowEnergy_Le1M.rst
	Configure_MultiEval_LowEnergy_Le2M.rst
	Configure_MultiEval_LowEnergy_Lrange.rst