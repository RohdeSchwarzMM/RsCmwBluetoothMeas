FilterPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LENergy[:LE1M]:FILTer:BWIDth

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LENergy[:LE1M]:FILTer:BWIDth



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.LowEnergy.Le1M.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: