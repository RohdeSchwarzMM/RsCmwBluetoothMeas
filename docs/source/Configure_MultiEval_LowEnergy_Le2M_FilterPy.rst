FilterPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LENergy:LE2M:FILTer:BWIDth

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LENergy:LE2M:FILTer:BWIDth



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.LowEnergy.Le2M.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: