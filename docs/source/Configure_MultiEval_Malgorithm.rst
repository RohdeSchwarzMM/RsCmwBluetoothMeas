Malgorithm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:MALGorithm:LENergy
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:MALGorithm:BRATe

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:MALGorithm:LENergy
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:MALGorithm:BRATe



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Malgorithm.MalgorithmCls
	:members:
	:undoc-members:
	:noindex: