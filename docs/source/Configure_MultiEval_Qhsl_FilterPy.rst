FilterPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:QHSL:FILTer:BWIDth

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:QHSL:FILTer:BWIDth



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Qhsl.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: