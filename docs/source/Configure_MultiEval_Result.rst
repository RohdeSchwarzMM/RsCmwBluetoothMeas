Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:SPOWer
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:PENCoding
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:FRANge
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:SGACp
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:SOBW
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:SACP
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult[:ALL]
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:PSCalar
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:IQABsolute
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:IQERror
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:IQDiff
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:PVTime
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:DEVMagnitude
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:PDIFference
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:MSCalar
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:FDEViation
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:PVSLot

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:SPOWer
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:PENCoding
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:FRANge
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:SGACp
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:SOBW
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:SACP
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult[:ALL]
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:PSCalar
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:IQABsolute
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:IQERror
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:IQDiff
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:PVTime
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:DEVMagnitude
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:PDIFference
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:MSCalar
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:FDEViation
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:RESult:PVSLot



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: