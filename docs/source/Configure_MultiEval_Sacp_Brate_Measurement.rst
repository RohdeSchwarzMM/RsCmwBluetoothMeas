Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SACP:BRATe:MEASurement:MODE

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SACP:BRATe:MEASurement:MODE



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Sacp.Brate.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: