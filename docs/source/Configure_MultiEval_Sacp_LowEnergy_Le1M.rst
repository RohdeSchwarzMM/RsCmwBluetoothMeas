Le1M
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Sacp.LowEnergy.Le1M.Le1MCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.sacp.lowEnergy.le1M.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Sacp_LowEnergy_Le1M_Measurement.rst