Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy:LE2M:MEASurement:MODE

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy:LE2M:MEASurement:MODE



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Sacp.LowEnergy.Le2M.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: