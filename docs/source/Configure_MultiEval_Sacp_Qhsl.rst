Qhsl
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Sacp.Qhsl.QhslCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.sacp.qhsl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Sacp_Qhsl_Measurement.rst