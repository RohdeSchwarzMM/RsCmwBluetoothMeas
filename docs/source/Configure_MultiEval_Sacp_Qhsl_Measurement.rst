Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:MEASurement:MODE

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:MEASurement:MODE



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Sacp.Qhsl.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: