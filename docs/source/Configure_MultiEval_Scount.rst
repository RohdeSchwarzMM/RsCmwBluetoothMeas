Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:PENCoding
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:FRANge
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:SGACp
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:SOBW
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:SACP
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:PVTime
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:MODulation

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:PENCoding
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:FRANge
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:SGACp
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:SOBW
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:SACP
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:PVTime
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCOunt:MODulation



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: