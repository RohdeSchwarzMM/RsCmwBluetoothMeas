Sgacp
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Sgacp.SgacpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.sgacp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Sgacp_Edrate.rst