Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SGACp:EDRate:MEASurement:MODE

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SGACp:EDRate:MEASurement:MODE



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Sgacp.Edrate.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: