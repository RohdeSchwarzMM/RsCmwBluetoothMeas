Synchronise
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SYNChronise

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SYNChronise



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.MultiEval.Synchronise.SynchroniseCls
	:members:
	:undoc-members:
	:noindex: