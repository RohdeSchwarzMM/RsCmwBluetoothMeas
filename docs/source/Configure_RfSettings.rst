RfSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:EATTenuation
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:UMARgin
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:FREQuency
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:RLEVel

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:EATTenuation
	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:UMARgin
	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:FREQuency
	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:RLEVel



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Cte.rst
	Configure_RfSettings_Dtx.rst
	Configure_RfSettings_Mchannel.rst
	Configure_RfSettings_Mmode.rst