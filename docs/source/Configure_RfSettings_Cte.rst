Cte
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RfSettings.Cte.CteCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.cte.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Cte_LowEnergy.rst