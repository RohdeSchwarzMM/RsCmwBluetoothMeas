LowEnergy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:CTE:LENergy:NANTenna
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:CTE:LENergy:ROFFset

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:CTE:LENergy:NANTenna
	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:CTE:LENergy:ROFFset



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RfSettings.Cte.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.cte.lowEnergy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Cte_LowEnergy_Aoffset.rst