Aoffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:CTE:LENergy:AOFFset

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:CTE:LENergy:AOFFset



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RfSettings.Cte.LowEnergy.Aoffset.AoffsetCls
	:members:
	:undoc-members:
	:noindex: