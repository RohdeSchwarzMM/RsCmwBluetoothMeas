Dtx
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:DTX:STERror
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:DTX:FOFFset
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:DTX:MINDex
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:DTX

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:DTX:STERror
	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:DTX:FOFFset
	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:DTX:MINDex
	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:DTX



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RfSettings.Dtx.DtxCls
	:members:
	:undoc-members:
	:noindex: