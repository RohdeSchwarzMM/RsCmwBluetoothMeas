Mmode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:MMODe

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:MMODe



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RfSettings.Mmode.MmodeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.mmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Mmode_Nmode.rst