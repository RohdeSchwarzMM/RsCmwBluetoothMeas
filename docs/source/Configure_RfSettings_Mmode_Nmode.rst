Nmode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:MMODe:NMODe:LENergy

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:MMODe:NMODe:LENergy



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RfSettings.Mmode.Nmode.NmodeCls
	:members:
	:undoc-members:
	:noindex: