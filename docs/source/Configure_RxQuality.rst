RxQuality
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:DOFFset
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SADDress
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SATYpe
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:ADETect
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:MMODe
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:GARB
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:AINDex

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:DOFFset
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SADDress
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SATYpe
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:ADETect
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:MMODe
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:GARB
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:AINDex



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RxQuality.RxQualityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rxQuality.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RxQuality_Eattenuation.rst
	Configure_RxQuality_Per.rst
	Configure_RxQuality_Route.rst
	Configure_RxQuality_Sensitivity.rst
	Configure_RxQuality_SpotCheck.rst