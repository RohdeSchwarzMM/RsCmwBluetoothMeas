Per
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:PER:LEVel
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:PER:TXPackets
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:PER:LIMit

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:PER:LEVel
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:PER:TXPackets
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:PER:LIMit



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RxQuality.Per.PerCls
	:members:
	:undoc-members:
	:noindex: