Route
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:ROUTe

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:ROUTe



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RxQuality.Route.RouteCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rxQuality.route.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RxQuality_Route_Usage.rst