All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:ROUTe:USAGe:ALL

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:ROUTe:USAGe:ALL



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RxQuality.Route.Usage.All.AllCls
	:members:
	:undoc-members:
	:noindex: