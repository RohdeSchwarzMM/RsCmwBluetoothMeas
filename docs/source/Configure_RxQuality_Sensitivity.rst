Sensitivity
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity:STARtlevel
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity:STEPsize
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity:RETRy

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity:STARtlevel
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity:STEPsize
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity:RETRy



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RxQuality.Sensitivity.SensitivityCls
	:members:
	:undoc-members:
	:noindex: