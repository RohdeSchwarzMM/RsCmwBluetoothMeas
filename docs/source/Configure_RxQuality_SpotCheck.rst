SpotCheck
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SPOTcheck:LEVel

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SPOTcheck:LEVel



.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.RxQuality.SpotCheck.SpotCheckCls
	:members:
	:undoc-members:
	:noindex: