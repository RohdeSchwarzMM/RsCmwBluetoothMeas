Trx
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Trx.TrxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.trx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Trx_Result.rst