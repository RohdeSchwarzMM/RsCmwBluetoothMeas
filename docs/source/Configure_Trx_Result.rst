Result
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Configure.Trx.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.trx.result.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Trx_Result_All.rst