Synchronise
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:BLUetooth:SYNChronise

.. code-block:: python

	DIAGnostic:BLUetooth:SYNChronise



.. autoclass:: RsCmwBluetoothMeas.Implementations.Diagnostic.Bluetooth.Synchronise.SynchroniseCls
	:members:
	:undoc-members:
	:noindex: