RfControl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:BLUetooth:MEASurement<Instance>:RFControl:TXENable

.. code-block:: python

	DIAGnostic:BLUetooth:MEASurement<Instance>:RFControl:TXENable



.. autoclass:: RsCmwBluetoothMeas.Implementations.Diagnostic.RfControl.RfControlCls
	:members:
	:undoc-members:
	:noindex: