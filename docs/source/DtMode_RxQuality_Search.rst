Search
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.DtMode.RxQuality.Search.SearchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.dtMode.rxQuality.search.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	DtMode_RxQuality_Search_Per.rst