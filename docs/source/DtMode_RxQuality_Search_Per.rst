Per
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER
	single: STOP:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER
	single: ABORt:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER

.. code-block:: python

	INITiate:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER
	STOP:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER
	ABORt:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER



.. autoclass:: RsCmwBluetoothMeas.Implementations.DtMode.RxQuality.Search.Per.PerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.dtMode.rxQuality.search.per.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	DtMode_RxQuality_Search_Per_LowEnergy.rst
	DtMode_RxQuality_Search_Per_State.rst