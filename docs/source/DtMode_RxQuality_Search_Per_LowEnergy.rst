LowEnergy
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.DtMode.RxQuality.Search.Per.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.dtMode.rxQuality.search.per.lowEnergy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	DtMode_RxQuality_Search_Per_LowEnergy_Le1M.rst
	DtMode_RxQuality_Search_Per_LowEnergy_Le2M.rst
	DtMode_RxQuality_Search_Per_LowEnergy_Lrange.rst