Le1M
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE1M
	single: FETCh:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE1M
	single: CALCulate:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE1M

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE1M
	FETCh:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE1M
	CALCulate:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE1M



.. autoclass:: RsCmwBluetoothMeas.Implementations.DtMode.RxQuality.Search.Per.LowEnergy.Le1M.Le1MCls
	:members:
	:undoc-members:
	:noindex: