Le2M
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE2M
	single: FETCh:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE2M
	single: CALCulate:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE2M

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE2M
	FETCh:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE2M
	CALCulate:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LE2M



.. autoclass:: RsCmwBluetoothMeas.Implementations.DtMode.RxQuality.Search.Per.LowEnergy.Le2M.Le2MCls
	:members:
	:undoc-members:
	:noindex: