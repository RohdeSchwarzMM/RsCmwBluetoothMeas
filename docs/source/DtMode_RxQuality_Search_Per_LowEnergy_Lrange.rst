Lrange
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LRANge
	single: FETCh:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LRANge
	single: CALCulate:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LRANge

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LRANge
	FETCh:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LRANge
	CALCulate:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:LENergy:LRANge



.. autoclass:: RsCmwBluetoothMeas.Implementations.DtMode.RxQuality.Search.Per.LowEnergy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex: