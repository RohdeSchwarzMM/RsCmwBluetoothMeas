State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:STATe

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PER:STATe



.. autoclass:: RsCmwBluetoothMeas.Implementations.DtMode.RxQuality.Search.Per.State.StateCls
	:members:
	:undoc-members:
	:noindex: