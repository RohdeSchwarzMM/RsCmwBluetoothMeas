Hdr
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ABORt:BLUetooth:MEASurement<Instance>:HDR
	single: STOP:BLUetooth:MEASurement<Instance>:HDR
	single: INITiate:BLUetooth:MEASurement<Instance>:HDR

.. code-block:: python

	ABORt:BLUetooth:MEASurement<Instance>:HDR
	STOP:BLUetooth:MEASurement<Instance>:HDR
	INITiate:BLUetooth:MEASurement<Instance>:HDR



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.HdrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdr_InputSignal.rst
	Hdr_Modulation.rst
	Hdr_Pencoding.rst
	Hdr_PowerVsTime.rst
	Hdr_Sgacp.rst
	Hdr_State.rst
	Hdr_Trace.rst