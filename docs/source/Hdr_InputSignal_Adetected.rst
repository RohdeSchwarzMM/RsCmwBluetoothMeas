Adetected
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.InputSignal.Adetected.AdetectedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdr.inputSignal.adetected.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdr_InputSignal_Adetected_Plength.rst
	Hdr_InputSignal_Adetected_Ptype.rst