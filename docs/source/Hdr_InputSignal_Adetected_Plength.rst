Plength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:ADETected:PLENgth

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:ADETected:PLENgth



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.InputSignal.Adetected.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex: