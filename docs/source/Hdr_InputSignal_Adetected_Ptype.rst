Ptype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:ADETected:PTYPe

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:ADETected:PTYPe



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.InputSignal.Adetected.Ptype.PtypeCls
	:members:
	:undoc-members:
	:noindex: