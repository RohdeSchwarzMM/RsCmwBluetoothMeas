Modulation
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdr.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdr_Modulation_Average.rst
	Hdr_Modulation_Current.rst
	Hdr_Modulation_Maximum.rst
	Hdr_Modulation_StandardDev.rst