Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:MODulation:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:MODulation:AVERage
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDR:MODulation:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:MODulation:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:HDR:MODulation:AVERage
	CALCulate:BLUetooth:MEASurement<Instance>:HDR:MODulation:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: