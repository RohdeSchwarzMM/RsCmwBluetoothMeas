Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:MODulation:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:MODulation:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDR:MODulation:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:MODulation:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDR:MODulation:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:HDR:MODulation:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: