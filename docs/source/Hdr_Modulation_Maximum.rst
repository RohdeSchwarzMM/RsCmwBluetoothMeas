Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:MODulation:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:MODulation:MAXimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDR:MODulation:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:MODulation:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:HDR:MODulation:MAXimum
	CALCulate:BLUetooth:MEASurement<Instance>:HDR:MODulation:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Modulation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: