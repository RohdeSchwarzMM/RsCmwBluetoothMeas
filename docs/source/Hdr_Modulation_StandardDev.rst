StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:MODulation:SDEViation
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:MODulation:SDEViation
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDR:MODulation:SDEViation

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:MODulation:SDEViation
	FETCh:BLUetooth:MEASurement<Instance>:HDR:MODulation:SDEViation
	CALCulate:BLUetooth:MEASurement<Instance>:HDR:MODulation:SDEViation



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: