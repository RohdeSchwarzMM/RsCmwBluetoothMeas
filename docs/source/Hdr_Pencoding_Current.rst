Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:PENCoding:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:PENCoding:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDR:PENCoding:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:PENCoding:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDR:PENCoding:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:HDR:PENCoding:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Pencoding.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: