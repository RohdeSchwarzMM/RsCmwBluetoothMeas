Ssequence
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Pencoding.Ssequence.SsequenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdr.pencoding.ssequence.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdr_Pencoding_Ssequence_Current.rst