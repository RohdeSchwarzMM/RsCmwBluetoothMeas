Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:PVTime:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:PVTime:AVERage
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDR:PVTime:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:PVTime:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:HDR:PVTime:AVERage
	CALCulate:BLUetooth:MEASurement<Instance>:HDR:PVTime:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.PowerVsTime.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: