Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:PVTime:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:PVTime:MAXimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDR:PVTime:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:PVTime:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:HDR:PVTime:MAXimum
	CALCulate:BLUetooth:MEASurement<Instance>:HDR:PVTime:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.PowerVsTime.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: