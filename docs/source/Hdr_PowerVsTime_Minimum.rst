Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:PVTime:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:PVTime:MINimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDR:PVTime:MINimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:PVTime:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:HDR:PVTime:MINimum
	CALCulate:BLUetooth:MEASurement<Instance>:HDR:PVTime:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.PowerVsTime.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: