Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:SGACp[:CURRent]
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:SGACp[:CURRent]
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDR:SGACp[:CURRent]

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:SGACp[:CURRent]
	FETCh:BLUetooth:MEASurement<Instance>:HDR:SGACp[:CURRent]
	CALCulate:BLUetooth:MEASurement<Instance>:HDR:SGACp[:CURRent]



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Sgacp.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: