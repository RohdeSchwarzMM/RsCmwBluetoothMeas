State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:STATe

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:HDR:STATe



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdr.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdr_State_All.rst