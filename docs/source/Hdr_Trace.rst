Trace
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdr.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdr_Trace_DevMagnitude.rst
	Hdr_Trace_IqAbs.rst
	Hdr_Trace_IqDifference.rst
	Hdr_Trace_IqError.rst
	Hdr_Trace_Pdifference.rst
	Hdr_Trace_PowerVsTime.rst
	Hdr_Trace_Sgacp.rst