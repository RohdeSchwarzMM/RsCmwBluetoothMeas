DevMagnitude
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Trace.DevMagnitude.DevMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdr.trace.devMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdr_Trace_DevMagnitude_Average.rst
	Hdr_Trace_DevMagnitude_Current.rst
	Hdr_Trace_DevMagnitude_Maximum.rst