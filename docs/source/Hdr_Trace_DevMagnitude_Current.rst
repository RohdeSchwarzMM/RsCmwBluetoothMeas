Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:DEVMagnitude:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:DEVMagnitude:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:DEVMagnitude:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:DEVMagnitude:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Trace.DevMagnitude.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: