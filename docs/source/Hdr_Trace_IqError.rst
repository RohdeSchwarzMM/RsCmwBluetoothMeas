IqError
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:IQERr
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:IQERr

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:IQERr
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:IQERr



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Trace.IqError.IqErrorCls
	:members:
	:undoc-members:
	:noindex: