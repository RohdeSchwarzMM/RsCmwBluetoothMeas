Pdifference
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Trace.Pdifference.PdifferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdr.trace.pdifference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdr_Trace_Pdifference_Average.rst
	Hdr_Trace_Pdifference_Current.rst
	Hdr_Trace_Pdifference_Maximum.rst