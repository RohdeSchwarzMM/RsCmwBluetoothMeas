Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PDIFference:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PDIFference:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PDIFference:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PDIFference:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Trace.Pdifference.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: