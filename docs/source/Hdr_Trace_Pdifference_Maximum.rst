Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PDIFference:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PDIFference:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PDIFference:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PDIFference:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Trace.Pdifference.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: