PowerVsTime
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Trace.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdr.trace.powerVsTime.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdr_Trace_PowerVsTime_Average.rst
	Hdr_Trace_PowerVsTime_Current.rst
	Hdr_Trace_PowerVsTime_Maximum.rst
	Hdr_Trace_PowerVsTime_Minimum.rst