Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:MINimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Trace.PowerVsTime.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: