Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Trace.Sgacp.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: