Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Trace.Sgacp.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: