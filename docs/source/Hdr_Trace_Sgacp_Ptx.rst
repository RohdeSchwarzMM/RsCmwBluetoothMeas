Ptx
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp[:PTX]
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp[:PTX]

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp[:PTX]
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp[:PTX]



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdr.Trace.Sgacp.Ptx.PtxCls
	:members:
	:undoc-members:
	:noindex: