Hdrp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ABORt:BLUetooth:MEASurement<Instance>:HDRP
	single: STOP:BLUetooth:MEASurement<Instance>:HDRP
	single: INITiate:BLUetooth:MEASurement<Instance>:HDRP

.. code-block:: python

	ABORt:BLUetooth:MEASurement<Instance>:HDRP
	STOP:BLUetooth:MEASurement<Instance>:HDRP
	INITiate:BLUetooth:MEASurement<Instance>:HDRP



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.HdrpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdrp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdrp_InputSignal.rst
	Hdrp_Modulation.rst
	Hdrp_PowerVsTime.rst
	Hdrp_Sacp.rst
	Hdrp_State.rst
	Hdrp_Trace.rst