Adetected
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.InputSignal.Adetected.AdetectedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdrp.inputSignal.adetected.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdrp_InputSignal_Adetected_Pcoding.rst
	Hdrp_InputSignal_Adetected_Plength.rst