Pcoding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:ADETected:PCODing

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:ADETected:PCODing



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.InputSignal.Adetected.Pcoding.PcodingCls
	:members:
	:undoc-members:
	:noindex: