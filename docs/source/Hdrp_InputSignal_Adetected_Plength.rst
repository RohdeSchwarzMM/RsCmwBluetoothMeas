Plength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:ADETected:PLENgth

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:ADETected:PLENgth



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.InputSignal.Adetected.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex: