Modulation
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdrp.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdrp_Modulation_Average.rst
	Hdrp_Modulation_Current.rst
	Hdrp_Modulation_Maximum.rst
	Hdrp_Modulation_StandardDev.rst