Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:MODulation:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:MODulation:AVERage
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDRP:MODulation:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:MODulation:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:MODulation:AVERage
	CALCulate:BLUetooth:MEASurement<Instance>:HDRP:MODulation:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: