Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:MODulation:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:MODulation:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDRP:MODulation:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:MODulation:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:MODulation:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:HDRP:MODulation:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: