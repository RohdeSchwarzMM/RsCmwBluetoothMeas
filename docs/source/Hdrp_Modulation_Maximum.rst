Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:MODulation:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:MODulation:MAXimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDRP:MODulation:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:MODulation:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:MODulation:MAXimum
	CALCulate:BLUetooth:MEASurement<Instance>:HDRP:MODulation:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Modulation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: