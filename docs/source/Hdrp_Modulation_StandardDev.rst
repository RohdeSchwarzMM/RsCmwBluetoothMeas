StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:MODulation:SDEViation
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:MODulation:SDEViation
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDRP:MODulation:SDEViation

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:MODulation:SDEViation
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:MODulation:SDEViation
	CALCulate:BLUetooth:MEASurement<Instance>:HDRP:MODulation:SDEViation



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: