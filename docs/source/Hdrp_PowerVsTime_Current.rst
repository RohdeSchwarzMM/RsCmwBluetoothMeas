Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:PVTime:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:PVTime:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDRP:PVTime:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:PVTime:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:PVTime:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:HDRP:PVTime:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: