Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:PVTime:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:PVTime:MAXimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDRP:PVTime:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:PVTime:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:PVTime:MAXimum
	CALCulate:BLUetooth:MEASurement<Instance>:HDRP:PVTime:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.PowerVsTime.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: