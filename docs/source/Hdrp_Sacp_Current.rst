Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:SACP[:CURRent]
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:SACP[:CURRent]
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDRP:SACP[:CURRent]

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:SACP[:CURRent]
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:SACP[:CURRent]
	CALCulate:BLUetooth:MEASurement<Instance>:HDRP:SACP[:CURRent]



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Sacp.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: