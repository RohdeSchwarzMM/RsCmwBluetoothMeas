State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:STATe

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:HDRP:STATe



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdrp.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdrp_State_All.rst