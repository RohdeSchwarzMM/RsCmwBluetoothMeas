Trace
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdrp.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdrp_Trace_EvMagnitude.rst
	Hdrp_Trace_IqAbs.rst
	Hdrp_Trace_IqOffset.rst
	Hdrp_Trace_PowerVsTime.rst
	Hdrp_Trace_Sacp.rst