EvMagnitude
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdrp.trace.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdrp_Trace_EvMagnitude_Absolute.rst
	Hdrp_Trace_EvMagnitude_Offset.rst