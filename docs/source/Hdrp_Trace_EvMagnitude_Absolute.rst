Absolute
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.EvMagnitude.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdrp.trace.evMagnitude.absolute.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdrp_Trace_EvMagnitude_Absolute_Average.rst
	Hdrp_Trace_EvMagnitude_Absolute_Current.rst
	Hdrp_Trace_EvMagnitude_Absolute_Maximum.rst