Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:ABSolute:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:ABSolute:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:ABSolute:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:ABSolute:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.EvMagnitude.Absolute.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: