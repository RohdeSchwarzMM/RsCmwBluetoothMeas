Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:ABSolute:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:ABSolute:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:ABSolute:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:ABSolute:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.EvMagnitude.Absolute.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: