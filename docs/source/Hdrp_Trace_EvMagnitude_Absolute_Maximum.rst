Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:ABSolute:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:ABSolute:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:ABSolute:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:ABSolute:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.EvMagnitude.Absolute.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: