Offset
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.EvMagnitude.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdrp.trace.evMagnitude.offset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdrp_Trace_EvMagnitude_Offset_Average.rst
	Hdrp_Trace_EvMagnitude_Offset_Current.rst
	Hdrp_Trace_EvMagnitude_Offset_Maximum.rst