Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:OFFSet:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:OFFSet:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:OFFSet:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:OFFSet:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.EvMagnitude.Offset.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: