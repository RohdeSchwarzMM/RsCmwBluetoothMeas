Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:OFFSet:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:OFFSet:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:OFFSet:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:OFFSet:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.EvMagnitude.Offset.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: