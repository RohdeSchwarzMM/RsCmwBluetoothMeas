Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:OFFSet:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:OFFSet:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:OFFSet:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:EVMagnitude:OFFSet:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.EvMagnitude.Offset.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: