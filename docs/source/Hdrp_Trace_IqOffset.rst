IqOffset
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:IQOFfset
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:IQOFfset

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:IQOFfset
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:IQOFfset



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.IqOffset.IqOffsetCls
	:members:
	:undoc-members:
	:noindex: