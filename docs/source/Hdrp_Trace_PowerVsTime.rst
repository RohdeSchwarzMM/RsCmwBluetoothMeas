PowerVsTime
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdrp.trace.powerVsTime.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdrp_Trace_PowerVsTime_Average.rst
	Hdrp_Trace_PowerVsTime_Current.rst
	Hdrp_Trace_PowerVsTime_Maximum.rst
	Hdrp_Trace_PowerVsTime_Minimum.rst