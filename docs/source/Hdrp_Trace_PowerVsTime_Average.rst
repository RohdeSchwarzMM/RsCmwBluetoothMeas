Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.PowerVsTime.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: