Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: