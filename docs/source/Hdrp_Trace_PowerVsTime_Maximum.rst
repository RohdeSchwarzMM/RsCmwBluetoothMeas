Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.PowerVsTime.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: