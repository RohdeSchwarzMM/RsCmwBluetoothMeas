Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:MINimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:PVTime:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.PowerVsTime.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: