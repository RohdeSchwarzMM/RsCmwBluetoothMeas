Sacp
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.Sacp.SacpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hdrp.trace.sacp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hdrp_Trace_Sacp_Average.rst
	Hdrp_Trace_Sacp_Current.rst
	Hdrp_Trace_Sacp_Maximum.rst
	Hdrp_Trace_Sacp_Ptx.rst