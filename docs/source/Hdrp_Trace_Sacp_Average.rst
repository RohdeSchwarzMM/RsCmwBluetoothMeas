Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:SACP:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:SACP:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:SACP:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:SACP:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.Sacp.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: