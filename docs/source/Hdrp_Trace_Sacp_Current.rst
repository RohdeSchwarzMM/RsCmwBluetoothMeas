Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:SACP:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:SACP:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:SACP:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:SACP:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.Sacp.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: