Ptx
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:SACP[:PTX]
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:SACP[:PTX]

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:TRACe:SACP[:PTX]
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:TRACe:SACP[:PTX]



.. autoclass:: RsCmwBluetoothMeas.Implementations.Hdrp.Trace.Sacp.Ptx.PtxCls
	:members:
	:undoc-members:
	:noindex: