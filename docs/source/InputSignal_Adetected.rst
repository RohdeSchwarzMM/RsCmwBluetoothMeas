Adetected
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.AdetectedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.inputSignal.adetected.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	InputSignal_Adetected_Aaddress.rst
	InputSignal_Adetected_Coding.rst
	InputSignal_Adetected_Cte.rst
	InputSignal_Adetected_NoSlots.rst
	InputSignal_Adetected_Pattern.rst
	InputSignal_Adetected_PduType.rst
	InputSignal_Adetected_Plength.rst
	InputSignal_Adetected_Ptype.rst
	InputSignal_Adetected_Qhsl.rst