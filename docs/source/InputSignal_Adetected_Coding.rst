Coding
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Coding.CodingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.inputSignal.adetected.coding.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	InputSignal_Adetected_Coding_LowEnergy.rst