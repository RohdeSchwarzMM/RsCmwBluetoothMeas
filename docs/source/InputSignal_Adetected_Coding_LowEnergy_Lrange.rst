Lrange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CODing:LENergy:LRANge

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CODing:LENergy:LRANge



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Coding.LowEnergy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex: