TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CTE:LENergy:LE2M:TYPE

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CTE:LENergy:LE2M:TYPE



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Cte.LowEnergy.Le2M.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: