Units
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CTE:LENergy:LE2M:UNITs

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CTE:LENergy:LE2M:UNITs



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Cte.LowEnergy.Le2M.Units.UnitsCls
	:members:
	:undoc-members:
	:noindex: