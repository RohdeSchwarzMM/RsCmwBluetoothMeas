TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CTE:QHSL:P2Q:TYPE

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CTE:QHSL:P2Q:TYPE



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Cte.Qhsl.P2Q.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: