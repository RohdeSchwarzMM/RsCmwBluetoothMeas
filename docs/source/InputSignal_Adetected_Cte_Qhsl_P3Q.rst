P3Q
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Cte.Qhsl.P3Q.P3QCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.inputSignal.adetected.cte.qhsl.p3Q.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	InputSignal_Adetected_Cte_Qhsl_P3Q_TypePy.rst
	InputSignal_Adetected_Cte_Qhsl_P3Q_Units.rst