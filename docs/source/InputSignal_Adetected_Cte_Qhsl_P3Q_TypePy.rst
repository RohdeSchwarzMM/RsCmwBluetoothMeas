TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CTE:QHSL:P3Q:TYPE

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CTE:QHSL:P3Q:TYPE



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Cte.Qhsl.P3Q.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: