P4Q
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Cte.Qhsl.P4Q.P4QCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.inputSignal.adetected.cte.qhsl.p4Q.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	InputSignal_Adetected_Cte_Qhsl_P4Q_TypePy.rst
	InputSignal_Adetected_Cte_Qhsl_P4Q_Units.rst