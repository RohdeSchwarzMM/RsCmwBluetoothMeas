Units
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CTE:QHSL:P5Q:UNITs

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CTE:QHSL:P5Q:UNITs



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Cte.Qhsl.P5Q.Units.UnitsCls
	:members:
	:undoc-members:
	:noindex: