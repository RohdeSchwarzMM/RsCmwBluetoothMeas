Brate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:NOSLots:BRATe

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:NOSLots:BRATe



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.NoSlots.Brate.BrateCls
	:members:
	:undoc-members:
	:noindex: