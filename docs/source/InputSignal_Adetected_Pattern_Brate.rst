Brate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PATTern[:BRATe]

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PATTern[:BRATe]



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Pattern.Brate.BrateCls
	:members:
	:undoc-members:
	:noindex: