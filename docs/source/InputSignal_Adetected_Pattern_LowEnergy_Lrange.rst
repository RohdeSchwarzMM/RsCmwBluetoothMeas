Lrange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PATTern:LENergy:LRANge

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PATTern:LENergy:LRANge



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Pattern.LowEnergy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex: