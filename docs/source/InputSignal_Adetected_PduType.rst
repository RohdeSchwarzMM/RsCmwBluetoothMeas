PduType
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.PduType.PduTypeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.inputSignal.adetected.pduType.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	InputSignal_Adetected_PduType_LowEnergy.rst