Plength
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.inputSignal.adetected.plength.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	InputSignal_Adetected_Plength_Brate.rst
	InputSignal_Adetected_Plength_Edrate.rst
	InputSignal_Adetected_Plength_LowEnergy.rst
	InputSignal_Adetected_Plength_Qhsl.rst