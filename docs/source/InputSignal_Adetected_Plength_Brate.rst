Brate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:BRATe

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:BRATe



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Plength.Brate.BrateCls
	:members:
	:undoc-members:
	:noindex: