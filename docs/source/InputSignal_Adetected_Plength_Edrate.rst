Edrate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:EDRate

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:EDRate



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Plength.Edrate.EdrateCls
	:members:
	:undoc-members:
	:noindex: