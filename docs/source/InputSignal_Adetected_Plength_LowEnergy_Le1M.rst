Le1M
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:LENergy[:LE1M]

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:LENergy[:LE1M]



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Plength.LowEnergy.Le1M.Le1MCls
	:members:
	:undoc-members:
	:noindex: