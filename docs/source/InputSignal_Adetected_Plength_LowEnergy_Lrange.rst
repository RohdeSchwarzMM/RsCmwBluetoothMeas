Lrange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:LENergy:LRANge

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:LENergy:LRANge



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Plength.LowEnergy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex: