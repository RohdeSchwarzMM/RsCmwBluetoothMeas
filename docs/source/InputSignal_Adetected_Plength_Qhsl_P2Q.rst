P2Q
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:QHSL:P2Q

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:QHSL:P2Q



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Plength.Qhsl.P2Q.P2QCls
	:members:
	:undoc-members:
	:noindex: