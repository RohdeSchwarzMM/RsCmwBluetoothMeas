P3Q
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:QHSL:P3Q

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:QHSL:P3Q



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Plength.Qhsl.P3Q.P3QCls
	:members:
	:undoc-members:
	:noindex: