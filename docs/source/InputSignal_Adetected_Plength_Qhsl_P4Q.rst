P4Q
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:QHSL:P4Q

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:QHSL:P4Q



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Plength.Qhsl.P4Q.P4QCls
	:members:
	:undoc-members:
	:noindex: