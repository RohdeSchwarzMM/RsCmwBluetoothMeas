P5Q
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:QHSL:P5Q

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:QHSL:P5Q



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Plength.Qhsl.P5Q.P5QCls
	:members:
	:undoc-members:
	:noindex: