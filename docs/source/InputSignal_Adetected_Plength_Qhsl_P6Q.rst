P6Q
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:QHSL:P6Q

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PLENgth:QHSL:P6Q



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Plength.Qhsl.P6Q.P6QCls
	:members:
	:undoc-members:
	:noindex: