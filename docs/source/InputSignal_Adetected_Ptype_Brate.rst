Brate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PTYPe:BRATe

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PTYPe:BRATe



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Ptype.Brate.BrateCls
	:members:
	:undoc-members:
	:noindex: