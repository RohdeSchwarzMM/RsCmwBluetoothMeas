Edrate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PTYPe:EDRate

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PTYPe:EDRate



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Ptype.Edrate.EdrateCls
	:members:
	:undoc-members:
	:noindex: