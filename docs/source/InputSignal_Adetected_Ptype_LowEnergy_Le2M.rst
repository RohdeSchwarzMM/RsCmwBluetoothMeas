Le2M
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PTYPe:LENergy:LE2M

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PTYPe:LENergy:LE2M



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Ptype.LowEnergy.Le2M.Le2MCls
	:members:
	:undoc-members:
	:noindex: