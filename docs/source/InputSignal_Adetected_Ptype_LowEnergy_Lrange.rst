Lrange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PTYPe:LENergy:LRANge

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:PTYPe:LENergy:LRANge



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Ptype.LowEnergy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex: