Phy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:QHSL:PHY

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:QHSL:PHY



.. autoclass:: RsCmwBluetoothMeas.Implementations.InputSignal.Adetected.Qhsl.Phy.PhyCls
	:members:
	:undoc-members:
	:noindex: