MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STOP:BLUetooth:MEASurement<Instance>:MEValuation
	single: ABORt:BLUetooth:MEASurement<Instance>:MEValuation
	single: INITiate:BLUetooth:MEASurement<Instance>:MEValuation

.. code-block:: python

	STOP:BLUetooth:MEASurement<Instance>:MEValuation
	ABORt:BLUetooth:MEASurement<Instance>:MEValuation
	INITiate:BLUetooth:MEASurement<Instance>:MEValuation



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Frange.rst
	MultiEval_ListPy.rst
	MultiEval_Modulation.rst
	MultiEval_Pencoding.rst
	MultiEval_PowerVsTime.rst
	MultiEval_Sacp.rst
	MultiEval_Sgacp.rst
	MultiEval_SoBw.rst
	MultiEval_State.rst
	MultiEval_Trace.rst