Brate
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Frange.Brate.BrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.frange.brate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Frange_Brate_Current.rst