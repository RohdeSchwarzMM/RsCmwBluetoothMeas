Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:FRANge:BRATe:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:FRANge:BRATe:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:FRANge:BRATe:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:FRANge:BRATe:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:FRANge:BRATe:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:FRANge:BRATe:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Frange.Brate.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: