Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S128
	rc = driver.multiEval.listPy.segment.repcap_segment_get()
	driver.multiEval.listPy.segment.repcap_segment_set(repcap.Segment.S1)





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_Modulation.rst
	MultiEval_ListPy_Segment_Pencoding.rst
	MultiEval_ListPy_Segment_PowerVsTime.rst
	MultiEval_ListPy_Segment_Sacp.rst
	MultiEval_ListPy_Segment_SoBw.rst