Extended
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:SDEViation:EXTended

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:SDEViation:EXTended



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.ListPy.Segment.Modulation.StandardDev.Extended.ExtendedCls
	:members:
	:undoc-members:
	:noindex: