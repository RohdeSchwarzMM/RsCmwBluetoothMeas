Extended
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:XMAXimum:EXTended

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:XMAXimum:EXTended



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.ListPy.Segment.Modulation.Xmaximum.Extended.ExtendedCls
	:members:
	:undoc-members:
	:noindex: