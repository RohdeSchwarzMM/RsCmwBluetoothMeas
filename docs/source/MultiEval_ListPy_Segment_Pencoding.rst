Pencoding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PENCoding

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PENCoding



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.ListPy.Segment.Pencoding.PencodingCls
	:members:
	:undoc-members:
	:noindex: