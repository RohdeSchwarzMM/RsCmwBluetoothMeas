PowerVsTime
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.ListPy.Segment.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.powerVsTime.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_PowerVsTime_Average.rst
	MultiEval_ListPy_Segment_PowerVsTime_Current.rst
	MultiEval_ListPy_Segment_PowerVsTime_Maximum.rst
	MultiEval_ListPy_Segment_PowerVsTime_Minimum.rst