Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PVTime:AVERage

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PVTime:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.ListPy.Segment.PowerVsTime.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: