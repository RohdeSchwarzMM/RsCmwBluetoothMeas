Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PVTime:MINimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PVTime:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.ListPy.Segment.PowerVsTime.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: