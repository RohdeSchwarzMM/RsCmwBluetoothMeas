Ptx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SACP[:PTX]

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SACP[:PTX]



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.ListPy.Segment.Sacp.Ptx.PtxCls
	:members:
	:undoc-members:
	:noindex: