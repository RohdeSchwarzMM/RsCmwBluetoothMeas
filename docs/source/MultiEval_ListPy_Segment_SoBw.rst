SoBw
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.ListPy.Segment.SoBw.SoBwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.soBw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_SoBw_Maximum.rst