Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SOBW:MAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SOBW:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.ListPy.Segment.SoBw.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: