Modulation
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Modulation_Brate.rst
	MultiEval_Modulation_Cte.rst
	MultiEval_Modulation_Edrate.rst
	MultiEval_Modulation_LowEnergy.rst
	MultiEval_Modulation_Nmode.rst
	MultiEval_Modulation_Qhsl.rst