Brate
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Brate.BrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.modulation.brate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Modulation_Brate_Average.rst
	MultiEval_Modulation_Brate_Current.rst
	MultiEval_Modulation_Brate_Maximum.rst
	MultiEval_Modulation_Brate_Minimum.rst
	MultiEval_Modulation_Brate_StandardDev.rst
	MultiEval_Modulation_Brate_Xmaximum.rst
	MultiEval_Modulation_Brate_Xminimum.rst
	MultiEval_Modulation_Brate_YieldPy.rst