Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Brate.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: