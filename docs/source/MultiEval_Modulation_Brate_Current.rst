Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Brate.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: