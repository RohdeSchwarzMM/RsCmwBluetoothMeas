Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Brate.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: