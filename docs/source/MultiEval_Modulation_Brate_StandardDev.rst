StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:SDEViation
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:SDEViation
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:SDEViation

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:SDEViation
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:SDEViation
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:SDEViation



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Brate.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: