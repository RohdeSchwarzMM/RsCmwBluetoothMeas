Xmaximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:XMAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:XMAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:XMAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:XMAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:XMAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:XMAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Brate.Xmaximum.XmaximumCls
	:members:
	:undoc-members:
	:noindex: