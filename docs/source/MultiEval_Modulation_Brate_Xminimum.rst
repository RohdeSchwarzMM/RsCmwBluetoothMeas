Xminimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:XMINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:XMINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:XMINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:XMINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:XMINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:XMINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Brate.Xminimum.XminimumCls
	:members:
	:undoc-members:
	:noindex: