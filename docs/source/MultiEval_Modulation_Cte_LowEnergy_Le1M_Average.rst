Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:CTE:LENergy:LE1M:AVERage
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:CTE:LENergy:LE1M:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:CTE:LENergy:LE1M:AVERage

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:CTE:LENergy:LE1M:AVERage
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:CTE:LENergy:LE1M:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:CTE:LENergy:LE1M:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Cte.LowEnergy.Le1M.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: