Xmaximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:CTE:LENergy:LE1M:XMAXimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:CTE:LENergy:LE1M:XMAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:CTE:LENergy:LE1M:XMAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:CTE:LENergy:LE1M:XMAXimum
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:CTE:LENergy:LE1M:XMAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:CTE:LENergy:LE1M:XMAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Cte.LowEnergy.Le1M.Xmaximum.XmaximumCls
	:members:
	:undoc-members:
	:noindex: