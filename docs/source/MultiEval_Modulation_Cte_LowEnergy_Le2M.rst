Le2M
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Cte.LowEnergy.Le2M.Le2MCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.modulation.cte.lowEnergy.le2M.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Modulation_Cte_LowEnergy_Le2M_Average.rst
	MultiEval_Modulation_Cte_LowEnergy_Le2M_Current.rst
	MultiEval_Modulation_Cte_LowEnergy_Le2M_Maximum.rst
	MultiEval_Modulation_Cte_LowEnergy_Le2M_StandardDev.rst
	MultiEval_Modulation_Cte_LowEnergy_Le2M_Xmaximum.rst
	MultiEval_Modulation_Cte_LowEnergy_Le2M_Xminimum.rst