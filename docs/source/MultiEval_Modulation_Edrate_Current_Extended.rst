Extended
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:CURRent:EXTended
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:CURRent:EXTended

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:CURRent:EXTended
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:CURRent:EXTended



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Edrate.Current.Extended.ExtendedCls
	:members:
	:undoc-members:
	:noindex: