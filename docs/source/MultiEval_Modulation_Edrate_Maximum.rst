Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Edrate.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: