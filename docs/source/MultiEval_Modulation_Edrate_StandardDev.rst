StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:SDEViation
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:SDEViation
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:SDEViation

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:SDEViation
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:SDEViation
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:SDEViation



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Edrate.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: