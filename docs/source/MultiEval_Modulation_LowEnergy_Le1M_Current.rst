Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:CURRent

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.LowEnergy.Le1M.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: