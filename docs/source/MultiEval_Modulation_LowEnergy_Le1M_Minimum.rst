Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:MINimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:MINimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:MINimum
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.LowEnergy.Le1M.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: