StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:SDEViation
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:SDEViation
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:SDEViation

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:SDEViation
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:SDEViation
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy[:LE1M]:SDEViation



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.LowEnergy.Le1M.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: