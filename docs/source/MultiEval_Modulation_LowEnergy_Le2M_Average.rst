Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:AVERage
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:AVERage

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:AVERage
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.LowEnergy.Le2M.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: