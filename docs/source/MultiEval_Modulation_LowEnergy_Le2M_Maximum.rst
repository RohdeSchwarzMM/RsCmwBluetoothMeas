Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:MAXimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:MAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:MAXimum
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.LowEnergy.Le2M.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: