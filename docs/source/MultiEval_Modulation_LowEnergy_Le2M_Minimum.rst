Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:MINimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:MINimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:MINimum
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.LowEnergy.Le2M.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: