Xmaximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:XMAXimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:XMAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:XMAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:XMAXimum
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:XMAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:XMAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.LowEnergy.Le2M.Xmaximum.XmaximumCls
	:members:
	:undoc-members:
	:noindex: