Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:CURRent

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.LowEnergy.Lrange.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: