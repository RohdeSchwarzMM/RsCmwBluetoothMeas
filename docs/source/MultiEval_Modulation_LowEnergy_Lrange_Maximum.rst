Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:MAXimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:MAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:MAXimum
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.LowEnergy.Lrange.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: