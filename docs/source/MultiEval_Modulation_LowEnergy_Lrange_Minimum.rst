Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:MINimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:MINimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:MINimum
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.LowEnergy.Lrange.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: