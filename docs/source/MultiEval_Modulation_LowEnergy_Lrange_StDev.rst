StDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:STDev
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:STDev
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:STDev

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:STDev
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:STDev
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:STDev



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.LowEnergy.Lrange.StDev.StDevCls
	:members:
	:undoc-members:
	:noindex: