Xminimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:XMINimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:XMINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:XMINimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:XMINimum
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:XMINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LRANge:XMINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.LowEnergy.Lrange.Xminimum.XminimumCls
	:members:
	:undoc-members:
	:noindex: