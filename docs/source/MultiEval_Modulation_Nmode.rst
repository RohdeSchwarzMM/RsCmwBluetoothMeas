Nmode
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Nmode.NmodeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.modulation.nmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Modulation_Nmode_Classic.rst
	MultiEval_Modulation_Nmode_LowEnergy.rst