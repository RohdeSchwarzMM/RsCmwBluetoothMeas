Classic
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Nmode.Classic.ClassicCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.modulation.nmode.classic.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Modulation_Nmode_Classic_Average.rst
	MultiEval_Modulation_Nmode_Classic_Current.rst
	MultiEval_Modulation_Nmode_Classic_Maximum.rst
	MultiEval_Modulation_Nmode_Classic_Minimum.rst
	MultiEval_Modulation_Nmode_Classic_StandardDev.rst
	MultiEval_Modulation_Nmode_Classic_Xmaximum.rst
	MultiEval_Modulation_Nmode_Classic_Xminimum.rst