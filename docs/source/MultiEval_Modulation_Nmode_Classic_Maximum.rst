Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Nmode.Classic.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: