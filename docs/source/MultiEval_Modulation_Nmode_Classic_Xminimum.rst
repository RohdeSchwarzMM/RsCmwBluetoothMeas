Xminimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:XMINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:XMINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:XMINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:XMINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:XMINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:XMINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Nmode.Classic.Xminimum.XminimumCls
	:members:
	:undoc-members:
	:noindex: