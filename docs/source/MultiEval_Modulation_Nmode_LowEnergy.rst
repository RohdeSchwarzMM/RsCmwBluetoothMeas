LowEnergy
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Nmode.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.modulation.nmode.lowEnergy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Modulation_Nmode_LowEnergy_Le1M.rst
	MultiEval_Modulation_Nmode_LowEnergy_Le2M.rst
	MultiEval_Modulation_Nmode_LowEnergy_Lrange.rst