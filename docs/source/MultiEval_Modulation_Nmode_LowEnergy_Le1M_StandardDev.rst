StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy[:LE1M]:SDEViation
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy[:LE1M]:SDEViation
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy[:LE1M]:SDEViation

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy[:LE1M]:SDEViation
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy[:LE1M]:SDEViation
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy[:LE1M]:SDEViation



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Nmode.LowEnergy.Le1M.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: