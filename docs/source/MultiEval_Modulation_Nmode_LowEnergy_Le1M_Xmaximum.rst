Xmaximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy[:LE1M]:XMAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy[:LE1M]:XMAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy[:LE1M]:XMAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy[:LE1M]:XMAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy[:LE1M]:XMAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy[:LE1M]:XMAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Nmode.LowEnergy.Le1M.Xmaximum.XmaximumCls
	:members:
	:undoc-members:
	:noindex: