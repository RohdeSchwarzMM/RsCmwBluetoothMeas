Lrange
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Nmode.LowEnergy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.modulation.nmode.lowEnergy.lrange.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Modulation_Nmode_LowEnergy_Lrange_Average.rst
	MultiEval_Modulation_Nmode_LowEnergy_Lrange_Current.rst
	MultiEval_Modulation_Nmode_LowEnergy_Lrange_Maximum.rst
	MultiEval_Modulation_Nmode_LowEnergy_Lrange_Minimum.rst
	MultiEval_Modulation_Nmode_LowEnergy_Lrange_StandardDev.rst
	MultiEval_Modulation_Nmode_LowEnergy_Lrange_StDev.rst
	MultiEval_Modulation_Nmode_LowEnergy_Lrange_Xmaximum.rst
	MultiEval_Modulation_Nmode_LowEnergy_Lrange_Xminimum.rst