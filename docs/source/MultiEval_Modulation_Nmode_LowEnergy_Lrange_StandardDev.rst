StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy:LRANge:SDEViation
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy:LRANge:SDEViation
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy:LRANge:SDEViation

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy:LRANge:SDEViation
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy:LRANge:SDEViation
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy:LRANge:SDEViation



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Nmode.LowEnergy.Lrange.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: