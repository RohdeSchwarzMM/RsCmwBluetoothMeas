Xmaximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy:LRANge:XMAXimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy:LRANge:XMAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy:LRANge:XMAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy:LRANge:XMAXimum
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy:LRANge:XMAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:LENergy:LRANge:XMAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Nmode.LowEnergy.Lrange.Xmaximum.XmaximumCls
	:members:
	:undoc-members:
	:noindex: