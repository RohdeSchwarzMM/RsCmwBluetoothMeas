Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P2Q:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P2Q:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P2Q:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P2Q:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P2Q:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P2Q:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Qhsl.P2Q.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: