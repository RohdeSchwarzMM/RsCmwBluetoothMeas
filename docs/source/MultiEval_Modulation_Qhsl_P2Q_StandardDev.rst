StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P2Q:SDEViation
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P2Q:SDEViation
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P2Q:SDEViation

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P2Q:SDEViation
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P2Q:SDEViation
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P2Q:SDEViation



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Qhsl.P2Q.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: