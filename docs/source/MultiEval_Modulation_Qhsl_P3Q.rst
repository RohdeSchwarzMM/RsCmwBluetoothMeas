P3Q
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Qhsl.P3Q.P3QCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.modulation.qhsl.p3Q.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Modulation_Qhsl_P3Q_Average.rst
	MultiEval_Modulation_Qhsl_P3Q_Current.rst
	MultiEval_Modulation_Qhsl_P3Q_Maximum.rst
	MultiEval_Modulation_Qhsl_P3Q_StandardDev.rst