Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P4Q:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P4Q:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P4Q:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P4Q:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P4Q:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P4Q:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Qhsl.P4Q.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: