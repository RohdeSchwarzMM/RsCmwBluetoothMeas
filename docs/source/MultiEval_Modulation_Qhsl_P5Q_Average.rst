Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Qhsl.P5Q.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: