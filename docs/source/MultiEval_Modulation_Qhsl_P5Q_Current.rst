Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Qhsl.P5Q.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: