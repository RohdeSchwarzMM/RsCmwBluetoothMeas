StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:SDEViation
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:SDEViation
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:SDEViation

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:SDEViation
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:SDEViation
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:SDEViation



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Qhsl.P5Q.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: