P6Q
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Qhsl.P6Q.P6QCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.modulation.qhsl.p6Q.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Modulation_Qhsl_P6Q_Average.rst
	MultiEval_Modulation_Qhsl_P6Q_Current.rst
	MultiEval_Modulation_Qhsl_P6Q_Maximum.rst
	MultiEval_Modulation_Qhsl_P6Q_StandardDev.rst