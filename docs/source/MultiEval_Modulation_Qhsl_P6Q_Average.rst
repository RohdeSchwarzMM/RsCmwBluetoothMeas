Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P6Q:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P6Q:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P6Q:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P6Q:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P6Q:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P6Q:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Qhsl.P6Q.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: