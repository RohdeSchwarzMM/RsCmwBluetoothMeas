StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P6Q:SDEViation
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P6Q:SDEViation
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P6Q:SDEViation

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P6Q:SDEViation
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P6Q:SDEViation
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P6Q:SDEViation



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Modulation.Qhsl.P6Q.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: