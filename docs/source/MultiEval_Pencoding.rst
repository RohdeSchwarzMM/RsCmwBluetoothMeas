Pencoding
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Pencoding.PencodingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.pencoding.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Pencoding_Edrate.rst
	MultiEval_Pencoding_Ssequence.rst