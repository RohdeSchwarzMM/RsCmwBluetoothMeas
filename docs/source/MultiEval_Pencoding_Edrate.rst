Edrate
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Pencoding.Edrate.EdrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.pencoding.edrate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Pencoding_Edrate_Current.rst