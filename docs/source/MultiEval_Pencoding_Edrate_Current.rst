Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:EDRate:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:EDRate:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:EDRate:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:EDRate:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:EDRate:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:EDRate:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Pencoding.Edrate.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.pencoding.edrate.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Pencoding_Edrate_Current_C.rst