C
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:EDRate:CURRent:C

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:EDRate:CURRent:C



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Pencoding.Edrate.Current.C.CCls
	:members:
	:undoc-members:
	:noindex: