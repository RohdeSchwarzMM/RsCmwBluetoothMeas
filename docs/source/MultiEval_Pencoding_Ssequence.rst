Ssequence
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Pencoding.Ssequence.SsequenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.pencoding.ssequence.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Pencoding_Ssequence_Edrate.rst