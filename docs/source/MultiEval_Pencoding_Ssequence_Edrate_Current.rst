Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:SSEQuence:EDRate:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:SSEQuence:EDRate:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:SSEQuence:EDRate:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:SSEQuence:EDRate:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:SSEQuence:EDRate:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PENCoding:SSEQuence:EDRate:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Pencoding.Ssequence.Edrate.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: