PowerVsTime
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_Brate.rst
	MultiEval_PowerVsTime_Edrate.rst
	MultiEval_PowerVsTime_LowEnergy.rst
	MultiEval_PowerVsTime_Nmode.rst
	MultiEval_PowerVsTime_Qhsl.rst