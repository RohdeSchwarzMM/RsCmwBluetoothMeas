Brate
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Brate.BrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.brate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_Brate_Average.rst
	MultiEval_PowerVsTime_Brate_Current.rst
	MultiEval_PowerVsTime_Brate_Maximum.rst
	MultiEval_PowerVsTime_Brate_Minimum.rst