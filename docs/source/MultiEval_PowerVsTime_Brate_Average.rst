Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Brate.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: