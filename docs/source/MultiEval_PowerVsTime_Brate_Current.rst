Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Brate.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: