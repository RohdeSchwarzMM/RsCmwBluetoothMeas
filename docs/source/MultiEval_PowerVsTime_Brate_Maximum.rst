Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Brate.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: