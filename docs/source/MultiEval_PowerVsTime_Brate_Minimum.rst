Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:MINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:BRATe:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Brate.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: