Edrate
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Edrate.EdrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.edrate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_Edrate_Average.rst
	MultiEval_PowerVsTime_Edrate_Current.rst
	MultiEval_PowerVsTime_Edrate_Maximum.rst
	MultiEval_PowerVsTime_Edrate_Minimum.rst