Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Edrate.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: