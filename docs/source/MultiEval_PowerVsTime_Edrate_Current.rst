Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Edrate.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: