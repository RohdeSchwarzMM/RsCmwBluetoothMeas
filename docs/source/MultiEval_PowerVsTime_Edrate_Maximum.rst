Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Edrate.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: