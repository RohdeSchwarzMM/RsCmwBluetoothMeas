Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:MINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:EDRate:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Edrate.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: