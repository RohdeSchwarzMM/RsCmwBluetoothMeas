Le1M
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.LowEnergy.Le1M.Le1MCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.lowEnergy.le1M.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_LowEnergy_Le1M_Average.rst
	MultiEval_PowerVsTime_LowEnergy_Le1M_Current.rst
	MultiEval_PowerVsTime_LowEnergy_Le1M_Maximum.rst
	MultiEval_PowerVsTime_LowEnergy_Le1M_Minimum.rst