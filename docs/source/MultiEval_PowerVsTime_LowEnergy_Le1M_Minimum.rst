Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy[:LE1M]:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy[:LE1M]:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy[:LE1M]:MINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy[:LE1M]:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy[:LE1M]:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy[:LE1M]:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.LowEnergy.Le1M.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: