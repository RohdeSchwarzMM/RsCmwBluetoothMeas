Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.LowEnergy.Le2M.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: