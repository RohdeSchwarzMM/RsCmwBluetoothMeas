Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:MINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.LowEnergy.Le2M.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: