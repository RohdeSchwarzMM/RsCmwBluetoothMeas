Lrange
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.LowEnergy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.lowEnergy.lrange.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_LowEnergy_Lrange_Average.rst
	MultiEval_PowerVsTime_LowEnergy_Lrange_Current.rst
	MultiEval_PowerVsTime_LowEnergy_Lrange_Maximum.rst
	MultiEval_PowerVsTime_LowEnergy_Lrange_Minimum.rst