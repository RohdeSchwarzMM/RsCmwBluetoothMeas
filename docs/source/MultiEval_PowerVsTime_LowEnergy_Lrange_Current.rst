Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.LowEnergy.Lrange.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: