Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:MINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.LowEnergy.Lrange.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: