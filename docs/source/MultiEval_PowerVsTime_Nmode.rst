Nmode
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Nmode.NmodeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.nmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_Nmode_Classic.rst
	MultiEval_PowerVsTime_Nmode_LowEnergy.rst