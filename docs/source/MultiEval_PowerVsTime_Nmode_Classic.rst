Classic
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Nmode.Classic.ClassicCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.nmode.classic.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_Nmode_Classic_Average.rst
	MultiEval_PowerVsTime_Nmode_Classic_Current.rst
	MultiEval_PowerVsTime_Nmode_Classic_Maximum.rst
	MultiEval_PowerVsTime_Nmode_Classic_Minimum.rst