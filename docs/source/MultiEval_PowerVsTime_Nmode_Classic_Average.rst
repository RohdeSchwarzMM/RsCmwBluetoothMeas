Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Nmode.Classic.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: