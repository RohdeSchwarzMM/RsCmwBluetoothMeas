Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Nmode.Classic.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: