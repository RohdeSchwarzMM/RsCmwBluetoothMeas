Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:MINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:CLASsic:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Nmode.Classic.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: