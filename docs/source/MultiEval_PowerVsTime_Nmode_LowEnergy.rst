LowEnergy
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Nmode.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.nmode.lowEnergy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_Nmode_LowEnergy_Le1M.rst
	MultiEval_PowerVsTime_Nmode_LowEnergy_Le2M.rst
	MultiEval_PowerVsTime_Nmode_LowEnergy_Lrange.rst