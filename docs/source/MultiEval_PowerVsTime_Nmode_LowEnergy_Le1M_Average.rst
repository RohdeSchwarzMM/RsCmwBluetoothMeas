Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy[:LE1M]:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy[:LE1M]:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy[:LE1M]:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy[:LE1M]:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy[:LE1M]:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy[:LE1M]:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Nmode.LowEnergy.Le1M.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: