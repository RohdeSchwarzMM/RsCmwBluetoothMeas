Le2M
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Nmode.LowEnergy.Le2M.Le2MCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.nmode.lowEnergy.le2M.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_Nmode_LowEnergy_Le2M_Average.rst
	MultiEval_PowerVsTime_Nmode_LowEnergy_Le2M_Current.rst
	MultiEval_PowerVsTime_Nmode_LowEnergy_Le2M_Maximum.rst
	MultiEval_PowerVsTime_Nmode_LowEnergy_Le2M_Minimum.rst