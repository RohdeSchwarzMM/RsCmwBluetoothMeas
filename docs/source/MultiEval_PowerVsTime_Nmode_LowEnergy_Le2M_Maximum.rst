Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy:LE2M:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy:LE2M:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy:LE2M:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy:LE2M:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy:LE2M:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy:LE2M:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Nmode.LowEnergy.Le2M.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: