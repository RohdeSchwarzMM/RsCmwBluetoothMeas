Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy:LRANge:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy:LRANge:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy:LRANge:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy:LRANge:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy:LRANge:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:NMODe:LENergy:LRANge:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Nmode.LowEnergy.Lrange.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: