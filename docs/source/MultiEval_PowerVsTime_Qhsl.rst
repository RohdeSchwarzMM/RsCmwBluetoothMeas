Qhsl
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.QhslCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.qhsl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_Qhsl_P2Q.rst
	MultiEval_PowerVsTime_Qhsl_P3Q.rst
	MultiEval_PowerVsTime_Qhsl_P4Q.rst
	MultiEval_PowerVsTime_Qhsl_P5Q.rst
	MultiEval_PowerVsTime_Qhsl_P6Q.rst