Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P2Q.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: