Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P2Q.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: