Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:MINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P2Q:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P2Q.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: