Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P3Q.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: