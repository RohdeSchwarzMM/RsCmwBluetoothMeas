Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P3Q.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: