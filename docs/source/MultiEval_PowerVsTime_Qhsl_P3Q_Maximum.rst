Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P3Q.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: