Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:MINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P3Q:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P3Q.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: