P4Q
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P4Q.P4QCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.qhsl.p4Q.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_Qhsl_P4Q_Average.rst
	MultiEval_PowerVsTime_Qhsl_P4Q_Current.rst
	MultiEval_PowerVsTime_Qhsl_P4Q_Maximum.rst
	MultiEval_PowerVsTime_Qhsl_P4Q_Minimum.rst