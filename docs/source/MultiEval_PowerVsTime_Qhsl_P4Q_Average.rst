Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P4Q.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: