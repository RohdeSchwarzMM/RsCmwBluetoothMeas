Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P4Q.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: