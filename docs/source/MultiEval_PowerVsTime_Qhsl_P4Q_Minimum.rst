Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:MINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P4Q:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P4Q.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: