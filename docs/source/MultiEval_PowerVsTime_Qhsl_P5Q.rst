P5Q
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P5Q.P5QCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.qhsl.p5Q.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_Qhsl_P5Q_Average.rst
	MultiEval_PowerVsTime_Qhsl_P5Q_Current.rst
	MultiEval_PowerVsTime_Qhsl_P5Q_Maximum.rst
	MultiEval_PowerVsTime_Qhsl_P5Q_Minimum.rst