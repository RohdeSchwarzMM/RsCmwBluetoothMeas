Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P5Q:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P5Q:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P5Q:MINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P5Q:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P5Q:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P5Q:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P5Q.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: