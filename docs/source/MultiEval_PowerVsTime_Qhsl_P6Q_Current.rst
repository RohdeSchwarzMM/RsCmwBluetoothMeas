Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P6Q.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: