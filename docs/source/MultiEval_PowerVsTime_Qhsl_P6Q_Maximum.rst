Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P6Q.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: