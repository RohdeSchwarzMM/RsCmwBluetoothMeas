Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:MINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:QHSL:P6Q:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.PowerVsTime.Qhsl.P6Q.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: