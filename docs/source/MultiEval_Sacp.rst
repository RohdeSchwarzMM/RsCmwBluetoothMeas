Sacp
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.SacpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.sacp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Sacp_Brate.rst
	MultiEval_Sacp_LowEnergy.rst
	MultiEval_Sacp_Nmode.rst
	MultiEval_Sacp_Qhsl.rst