Brate
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.Brate.BrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.sacp.brate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Sacp_Brate_Ptx.rst