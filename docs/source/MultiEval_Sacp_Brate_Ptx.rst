Ptx
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:BRATe[:PTX]
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:BRATe[:PTX]
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:BRATe[:PTX]

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:BRATe[:PTX]
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:BRATe[:PTX]
	READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:BRATe[:PTX]



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.Brate.Ptx.PtxCls
	:members:
	:undoc-members:
	:noindex: