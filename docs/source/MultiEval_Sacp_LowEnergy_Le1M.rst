Le1M
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy[:LE1M]
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy[:LE1M]
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy[:LE1M]

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy[:LE1M]
	READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy[:LE1M]
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy[:LE1M]



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.LowEnergy.Le1M.Le1MCls
	:members:
	:undoc-members:
	:noindex: