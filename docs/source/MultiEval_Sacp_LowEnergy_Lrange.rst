Lrange
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy:LRANge
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy:LRANge
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy:LRANge

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy:LRANge
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy:LRANge
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:LENergy:LRANge



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.LowEnergy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex: