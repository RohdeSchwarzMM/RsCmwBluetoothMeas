Nmode
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.Nmode.NmodeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.sacp.nmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Sacp_Nmode_Classic.rst
	MultiEval_Sacp_Nmode_LowEnergy.rst