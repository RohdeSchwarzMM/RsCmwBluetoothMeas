Classic
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:CLASsic
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:CLASsic
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:CLASsic

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:CLASsic
	READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:CLASsic
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:CLASsic



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.Nmode.Classic.ClassicCls
	:members:
	:undoc-members:
	:noindex: