Le2M
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:LENergy:LE2M
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:LENergy:LE2M
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:LENergy:LE2M

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:LENergy:LE2M
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:LENergy:LE2M
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:LENergy:LE2M



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.Nmode.LowEnergy.Le2M.Le2MCls
	:members:
	:undoc-members:
	:noindex: