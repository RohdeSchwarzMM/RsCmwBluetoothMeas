Lrange
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:LENergy:LRANge
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:LENergy:LRANge
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:LENergy:LRANge

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:LENergy:LRANge
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:LENergy:LRANge
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:NMODe:LENergy:LRANge



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.Nmode.LowEnergy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex: