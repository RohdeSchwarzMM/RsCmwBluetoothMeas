P2Q
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P2Q
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P2Q
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P2Q

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P2Q
	READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P2Q
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P2Q



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.Qhsl.P2Q.P2QCls
	:members:
	:undoc-members:
	:noindex: