P4Q
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P4Q
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P4Q
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P4Q

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P4Q
	READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P4Q
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P4Q



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.Qhsl.P4Q.P4QCls
	:members:
	:undoc-members:
	:noindex: