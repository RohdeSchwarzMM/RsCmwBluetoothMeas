P5Q
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P5Q
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P5Q
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P5Q

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P5Q
	READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P5Q
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P5Q



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.Qhsl.P5Q.P5QCls
	:members:
	:undoc-members:
	:noindex: