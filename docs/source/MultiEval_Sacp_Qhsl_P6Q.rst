P6Q
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P6Q
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P6Q
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P6Q

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P6Q
	READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P6Q
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P6Q



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sacp.Qhsl.P6Q.P6QCls
	:members:
	:undoc-members:
	:noindex: