Sgacp
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sgacp.SgacpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.sgacp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Sgacp_Edrate.rst