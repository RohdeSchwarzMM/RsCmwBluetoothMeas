Edrate
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Sgacp.Edrate.EdrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.sgacp.edrate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Sgacp_Edrate_Ptx.rst