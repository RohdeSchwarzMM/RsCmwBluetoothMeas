Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SOBW:BRATe:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SOBW:BRATe:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SOBW:BRATe:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SOBW:BRATe:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:SOBW:BRATe:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SOBW:BRATe:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.SoBw.Brate.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: