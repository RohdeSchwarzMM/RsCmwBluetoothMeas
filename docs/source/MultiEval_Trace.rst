Trace
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_DevMagnitude.rst
	MultiEval_Trace_Fdeviation.rst
	MultiEval_Trace_Frange.rst
	MultiEval_Trace_IqAbs.rst
	MultiEval_Trace_IqDifference.rst
	MultiEval_Trace_IqError.rst
	MultiEval_Trace_Pdeviation.rst
	MultiEval_Trace_Pdifference.rst
	MultiEval_Trace_PowerVsTime.rst
	MultiEval_Trace_Sacp.rst
	MultiEval_Trace_Sgacp.rst
	MultiEval_Trace_SoBw.rst
	MultiEval_Trace_Spower.rst