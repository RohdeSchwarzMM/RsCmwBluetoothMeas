DevMagnitude
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.DevMagnitude.DevMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.devMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_DevMagnitude_Average.rst
	MultiEval_Trace_DevMagnitude_Current.rst
	MultiEval_Trace_DevMagnitude_Maximum.rst