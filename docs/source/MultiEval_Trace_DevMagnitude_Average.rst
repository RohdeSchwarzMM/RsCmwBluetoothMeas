Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:DEVMagnitude:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:DEVMagnitude:AVERage

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:DEVMagnitude:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:DEVMagnitude:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.DevMagnitude.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: