Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:DEVMagnitude:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:DEVMagnitude:CURRent

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:DEVMagnitude:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:DEVMagnitude:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.DevMagnitude.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: