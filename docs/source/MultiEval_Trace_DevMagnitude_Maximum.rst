Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:DEVMagnitude:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:DEVMagnitude:MAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:DEVMagnitude:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:DEVMagnitude:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.DevMagnitude.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: