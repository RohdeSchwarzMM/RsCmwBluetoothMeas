Fdeviation
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Fdeviation.FdeviationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.fdeviation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Fdeviation_Average.rst
	MultiEval_Trace_Fdeviation_Current.rst
	MultiEval_Trace_Fdeviation_Maximum.rst
	MultiEval_Trace_Fdeviation_Minimum.rst