Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:CURRent

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Fdeviation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: