Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:MINimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Fdeviation.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: