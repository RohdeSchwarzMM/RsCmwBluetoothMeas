Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FRANge:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FRANge:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FRANge:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FRANge:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Frange.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: