IqError
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:IQERr
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:IQERr

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:IQERr
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:IQERr



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.IqError.IqErrorCls
	:members:
	:undoc-members:
	:noindex: