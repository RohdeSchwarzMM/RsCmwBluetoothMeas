Pdeviation
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Pdeviation.PdeviationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.pdeviation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Pdeviation_Maximum.rst
	MultiEval_Trace_Pdeviation_Minimum.rst