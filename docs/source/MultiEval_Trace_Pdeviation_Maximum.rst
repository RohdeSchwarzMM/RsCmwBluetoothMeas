Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDEViation:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDEViation:MAXimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDEViation:MAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDEViation:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDEViation:MAXimum
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDEViation:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Pdeviation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: