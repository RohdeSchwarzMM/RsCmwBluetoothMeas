Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDEViation:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDEViation:MINimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDEViation:MINimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDEViation:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDEViation:MINimum
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDEViation:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Pdeviation.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: