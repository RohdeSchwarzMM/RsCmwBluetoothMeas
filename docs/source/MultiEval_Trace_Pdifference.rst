Pdifference
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Pdifference.PdifferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.pdifference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Pdifference_Average.rst
	MultiEval_Trace_Pdifference_Current.rst
	MultiEval_Trace_Pdifference_Maximum.rst