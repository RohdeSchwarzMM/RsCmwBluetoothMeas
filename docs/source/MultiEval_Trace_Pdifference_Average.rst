Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDIFference:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDIFference:AVERage

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDIFference:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDIFference:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Pdifference.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: