Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDIFference:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDIFference:MAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDIFference:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDIFference:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Pdifference.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: