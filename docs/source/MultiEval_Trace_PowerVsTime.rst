PowerVsTime
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.powerVsTime.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_PowerVsTime_Average.rst
	MultiEval_Trace_PowerVsTime_Current.rst
	MultiEval_Trace_PowerVsTime_Maximum.rst
	MultiEval_Trace_PowerVsTime_Minimum.rst