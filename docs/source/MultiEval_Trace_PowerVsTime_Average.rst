Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:AVERage

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.PowerVsTime.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: