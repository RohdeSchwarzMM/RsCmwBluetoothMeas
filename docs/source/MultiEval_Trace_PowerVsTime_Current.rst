Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:CURRent

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: