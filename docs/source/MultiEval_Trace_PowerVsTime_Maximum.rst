Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.PowerVsTime.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: