Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Sacp.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: