Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Sacp.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: