Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Sacp.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: