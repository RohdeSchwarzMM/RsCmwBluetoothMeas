Ptx
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP[:PTX]
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP[:PTX]

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP[:PTX]
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SACP[:PTX]



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Sacp.Ptx.PtxCls
	:members:
	:undoc-members:
	:noindex: