Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp:AVERage

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Sgacp.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: