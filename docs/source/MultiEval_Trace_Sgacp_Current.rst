Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp:CURRent

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Sgacp.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: