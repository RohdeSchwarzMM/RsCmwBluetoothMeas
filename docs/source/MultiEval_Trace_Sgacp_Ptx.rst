Ptx
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp[:PTX]
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp[:PTX]

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp[:PTX]
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp[:PTX]



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Sgacp.Ptx.PtxCls
	:members:
	:undoc-members:
	:noindex: