SoBw
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.SoBw.SoBwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.soBw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_SoBw_Average.rst
	MultiEval_Trace_SoBw_Current.rst
	MultiEval_Trace_SoBw_Maximum.rst