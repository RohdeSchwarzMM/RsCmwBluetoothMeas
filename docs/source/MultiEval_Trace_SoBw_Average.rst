Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SOBW:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SOBW:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SOBW:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SOBW:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.SoBw.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: