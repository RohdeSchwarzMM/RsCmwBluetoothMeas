Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SOBW:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SOBW:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SOBW:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SOBW:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.SoBw.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: