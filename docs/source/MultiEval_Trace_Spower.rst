Spower
----------------------------------------





.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Spower.SpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.spower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Spower_Average.rst
	MultiEval_Trace_Spower_Current.rst
	MultiEval_Trace_Spower_Maximum.rst
	MultiEval_Trace_Spower_Minimum.rst