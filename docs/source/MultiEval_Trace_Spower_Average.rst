Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:AVERage

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:AVERage



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Spower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: