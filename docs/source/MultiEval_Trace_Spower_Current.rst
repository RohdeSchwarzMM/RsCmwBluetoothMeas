Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:CURRent

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:CURRent



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Spower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: