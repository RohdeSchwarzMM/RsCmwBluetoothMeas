Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:MAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:MAXimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Spower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: