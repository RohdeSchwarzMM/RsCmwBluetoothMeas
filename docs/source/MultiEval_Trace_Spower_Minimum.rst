Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:MINimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SPOWer:MINimum



.. autoclass:: RsCmwBluetoothMeas.Implementations.MultiEval.Trace.Spower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: