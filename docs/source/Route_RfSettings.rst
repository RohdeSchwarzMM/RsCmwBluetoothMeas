RfSettings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:BLUetooth:MEASurement<Instance>:RFSettings:CONNector

.. code-block:: python

	ROUTe:BLUetooth:MEASurement<Instance>:RFSettings:CONNector



.. autoclass:: RsCmwBluetoothMeas.Implementations.Route.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: