MaProtocol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:BLUetooth:MEASurement<Instance>:SCENario:MAPRotocol

.. code-block:: python

	ROUTe:BLUetooth:MEASurement<Instance>:SCENario:MAPRotocol



.. autoclass:: RsCmwBluetoothMeas.Implementations.Route.Scenario.MaProtocol.MaProtocolCls
	:members:
	:undoc-members:
	:noindex: