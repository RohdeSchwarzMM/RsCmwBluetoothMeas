Salone
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:BLUetooth:MEASurement<Instance>:SCENario:SALone

.. code-block:: python

	ROUTe:BLUetooth:MEASurement<Instance>:SCENario:SALone



.. autoclass:: RsCmwBluetoothMeas.Implementations.Route.Scenario.Salone.SaloneCls
	:members:
	:undoc-members:
	:noindex: