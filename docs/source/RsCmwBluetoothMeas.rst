RsCmwBluetoothMeas API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCmwBluetoothMeas('TCPIP::192.168.2.101::hislip0')
	# Instance range: Inst1 .. Inst16
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCmwBluetoothMeas.RsCmwBluetoothMeas
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Call.rst
	Clean.rst
	Configure.rst
	Diagnostic.rst
	DtMode.rst
	Hdr.rst
	Hdrp.rst
	InputSignal.rst
	MultiEval.rst
	Route.rst
	RxQuality.rst
	Sense.rst
	Trigger.rst
	Trx.rst