RxQuality
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:BLUetooth:MEASurement<Instance>:RXQuality
	single: STOP:BLUetooth:MEASurement<Instance>:RXQuality
	single: ABORt:BLUetooth:MEASurement<Instance>:RXQuality

.. code-block:: python

	INITiate:BLUetooth:MEASurement<Instance>:RXQuality
	STOP:BLUetooth:MEASurement<Instance>:RXQuality
	ABORt:BLUetooth:MEASurement<Instance>:RXQuality



.. autoclass:: RsCmwBluetoothMeas.Implementations.RxQuality.RxQualityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Adetected.rst
	RxQuality_Per.rst
	RxQuality_Sensitivity.rst
	RxQuality_SpotCheck.rst
	RxQuality_State.rst