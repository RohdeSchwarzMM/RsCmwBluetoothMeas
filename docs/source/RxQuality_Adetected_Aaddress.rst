Aaddress
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:RXQuality:ADETected:AADDress

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:RXQuality:ADETected:AADDress



.. autoclass:: RsCmwBluetoothMeas.Implementations.RxQuality.Adetected.Aaddress.AaddressCls
	:members:
	:undoc-members:
	:noindex: