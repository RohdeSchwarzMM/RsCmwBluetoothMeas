Per
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:RXQuality:PER

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:RXQuality:PER



.. autoclass:: RsCmwBluetoothMeas.Implementations.RxQuality.Per.PerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.per.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Per_RxPackets.rst