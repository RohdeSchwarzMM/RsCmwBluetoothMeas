RxPackets
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:RXQuality:PER:RXPackets

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:RXQuality:PER:RXPackets



.. autoclass:: RsCmwBluetoothMeas.Implementations.RxQuality.Per.RxPackets.RxPacketsCls
	:members:
	:undoc-members:
	:noindex: