SpotCheck
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:RXQuality:SPOTcheck

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:RXQuality:SPOTcheck



.. autoclass:: RsCmwBluetoothMeas.Implementations.RxQuality.SpotCheck.SpotCheckCls
	:members:
	:undoc-members:
	:noindex: