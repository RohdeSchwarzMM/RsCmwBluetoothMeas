Elogging
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:BLUetooth:MEASurement<Instance>:ELOGging:LAST
	single: SENSe:BLUetooth:MEASurement<Instance>:ELOGging:ALL

.. code-block:: python

	SENSe:BLUetooth:MEASurement<Instance>:ELOGging:LAST
	SENSe:BLUetooth:MEASurement<Instance>:ELOGging:ALL



.. autoclass:: RsCmwBluetoothMeas.Implementations.Sense.Elogging.EloggingCls
	:members:
	:undoc-members:
	:noindex: