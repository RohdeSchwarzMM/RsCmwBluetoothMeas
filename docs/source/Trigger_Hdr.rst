Hdr
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:BLUetooth:MEASurement<Instance>:HDR:THReshold
	single: TRIGger:BLUetooth:MEASurement<Instance>:HDR:TOUT
	single: TRIGger:BLUetooth:MEASurement<Instance>:HDR:SOURce

.. code-block:: python

	TRIGger:BLUetooth:MEASurement<Instance>:HDR:THReshold
	TRIGger:BLUetooth:MEASurement<Instance>:HDR:TOUT
	TRIGger:BLUetooth:MEASurement<Instance>:HDR:SOURce



.. autoclass:: RsCmwBluetoothMeas.Implementations.Trigger.Hdr.HdrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.hdr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Hdr_Catalog.rst