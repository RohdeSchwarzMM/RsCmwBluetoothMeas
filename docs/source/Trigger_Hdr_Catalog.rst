Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BLUetooth:MEASurement<Instance>:HDR:CATalog:SOURce

.. code-block:: python

	TRIGger:BLUetooth:MEASurement<Instance>:HDR:CATalog:SOURce



.. autoclass:: RsCmwBluetoothMeas.Implementations.Trigger.Hdr.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: