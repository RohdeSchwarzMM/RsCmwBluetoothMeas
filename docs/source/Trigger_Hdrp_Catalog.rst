Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BLUetooth:MEASurement<Instance>:HDRP:CATalog:SOURce

.. code-block:: python

	TRIGger:BLUetooth:MEASurement<Instance>:HDRP:CATalog:SOURce



.. autoclass:: RsCmwBluetoothMeas.Implementations.Trigger.Hdrp.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: