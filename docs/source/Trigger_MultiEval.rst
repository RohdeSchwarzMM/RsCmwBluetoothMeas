MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:BLUetooth:MEASurement<Instance>:MEValuation:THReshold
	single: TRIGger:BLUetooth:MEASurement<Instance>:MEValuation:TOUT

.. code-block:: python

	TRIGger:BLUetooth:MEASurement<Instance>:MEValuation:THReshold
	TRIGger:BLUetooth:MEASurement<Instance>:MEValuation:TOUT



.. autoclass:: RsCmwBluetoothMeas.Implementations.Trigger.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex: