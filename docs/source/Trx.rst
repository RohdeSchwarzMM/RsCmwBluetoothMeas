Trx
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:BLUetooth:MEASurement<Instance>:TRX
	single: STOP:BLUetooth:MEASurement<Instance>:TRX
	single: ABORt:BLUetooth:MEASurement<Instance>:TRX

.. code-block:: python

	INITiate:BLUetooth:MEASurement<Instance>:TRX
	STOP:BLUetooth:MEASurement<Instance>:TRX
	ABORt:BLUetooth:MEASurement<Instance>:TRX



.. autoclass:: RsCmwBluetoothMeas.Implementations.Trx.TrxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trx_Acp.rst
	Trx_Modulation.rst
	Trx_Power.rst
	Trx_Spot.rst
	Trx_State.rst