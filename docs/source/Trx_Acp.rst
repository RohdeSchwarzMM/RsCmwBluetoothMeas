Acp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:TRX:ACP

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:TRX:ACP



.. autoclass:: RsCmwBluetoothMeas.Implementations.Trx.Acp.AcpCls
	:members:
	:undoc-members:
	:noindex: