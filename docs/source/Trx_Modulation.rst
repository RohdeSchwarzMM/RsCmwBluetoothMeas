Modulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:TRX:MODulation

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:TRX:MODulation



.. autoclass:: RsCmwBluetoothMeas.Implementations.Trx.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex: