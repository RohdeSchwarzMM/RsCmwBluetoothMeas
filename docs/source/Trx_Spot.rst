Spot
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:TRX:SPOT

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:TRX:SPOT



.. autoclass:: RsCmwBluetoothMeas.Implementations.Trx.Spot.SpotCls
	:members:
	:undoc-members:
	:noindex: