Enums
=========

AddressType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AddressType.PUBLic
	# All values (2x):
	PUBLic | RANDom

AutoManualMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManualMode.AUTO
	# All values (2x):
	AUTO | MANual

BaudRate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BaudRate.B110
	# Last value:
	value = enums.BaudRate.B96K
	# All values (24x):
	B110 | B115k | B12K | B14K | B19K | B1M | B1M5 | B234k
	B24K | B28K | B2M | B300 | B38K | B3M | B3M5 | B460k
	B48K | B4M | B500k | B576k | B57K | B600 | B921k | B96K

BrEdrChannelsRange
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BrEdrChannelsRange.CH21
	# All values (2x):
	CH21 | CH79

BrPacketType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BrPacketType.DH1
	# All values (3x):
	DH1 | DH3 | DH5

BurstType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BurstType.BR
	# All values (4x):
	BR | EDR | LE | QHSL

CmwSingleConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CmwSingleConnector.R11
	# Last value:
	value = enums.CmwSingleConnector.RB8
	# All values (48x):
	R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18
	R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28
	R31 | R32 | R33 | R34 | R35 | R36 | R37 | R38
	R41 | R42 | R43 | R44 | R45 | R46 | R47 | R48
	RA1 | RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8
	RB1 | RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8

CodingScheme
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingScheme.S2
	# All values (2x):
	S2 | S8

CommProtocol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CommProtocol.HCI
	# All values (2x):
	HCI | TWO

CtePacketType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CtePacketType.AOA1us
	# All values (5x):
	AOA1us | AOA2us | AOAus | AOD1us | AOD2us

CteType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CteType.AOA
	# All values (3x):
	AOA | AOD1 | AOD2

DataBits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DataBits.D7
	# All values (2x):
	D7 | D8

DetectedPatternType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DetectedPatternType.ALTernating
	# All values (4x):
	ALTernating | OTHer | P11 | P44

DetectedPhyTypeIsignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DetectedPhyTypeIsignal.P2Q
	# All values (5x):
	P2Q | P3Q | P4Q | P5Q | P6Q

DisplayMeasurement
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DisplayMeasurement.MEV
	# All values (1x):
	MEV

DisplayView
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DisplayView.DEVM
	# Last value:
	value = enums.DisplayView.SOBW
	# All values (15x):
	DEVM | FDEViation | FRANge | IQABs | IQDiff | IQERr | MODulation | OVERview
	PDIFference | PENCoding | POWer | PVTime | SACP | SGACp | SOBW

EdrPacketType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EdrPacketType.E21P
	# All values (6x):
	E21P | E23P | E25P | E31P | E33P | E35P

FilterWidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterWidth.NARRow
	# All values (2x):
	NARRow | WIDE

HwInterface
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HwInterface.NONE
	# All values (3x):
	NONE | RS232 | USB

LeChannelsRange
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LeChannelsRange.CH10
	# All values (2x):
	CH10 | CH40

LePacketType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LePacketType.ADVertiser
	# All values (3x):
	ADVertiser | RFCTe | RFPHytest

LePatternType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LePatternType.OTHer
	# All values (3x):
	OTHer | P11 | P44

LePhysicalType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LePhysicalType.LE1M
	# All values (3x):
	LE1M | LE2M | LELR

LePhysicalTypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LePhysicalTypeB.LE1M
	# All values (8x):
	LE1M | LE2M | LELR | P2Q | P3Q | P4Q | P5Q | P6Q

LeRangePaternType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LeRangePaternType.ALL0
	# All values (6x):
	ALL0 | ALL1 | OTHer | P11 | P44 | PRBS9

LeSymolTimeError
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LeSymolTimeError.NEG50
	# All values (3x):
	NEG50 | OFF | POS50

LogCategory
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogCategory.CONTinue
	# All values (4x):
	CONTinue | ERRor | INFO | WARNing

MeasureScope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasureScope.ALL
	# All values (2x):
	ALL | SINGle

MevPatternType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MevPatternType.ALL1
	# All values (5x):
	ALL1 | ALTernating | OTHer | P11 | P44

PacketTypeIsignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PacketTypeIsignal.H41P
	# All values (6x):
	H41P | H43P | H45P | H81P | H83P | H85P

PacketTypeQhsl
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PacketTypeQhsl.DATA
	# All values (1x):
	DATA

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

Parity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Parity.EVEN
	# All values (3x):
	EVEN | NONE | ODD

PatternIndependent
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PatternIndependent.PINDependent
	# All values (2x):
	PINDependent | SPECconform

PatternType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PatternType.ALL0
	# All values (5x):
	ALL0 | ALL1 | P11 | P44 | PRBS9

PatternTypeIsignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PatternTypeIsignal.OTHer
	# All values (2x):
	OTHer | PRBS9

PayloadCoding
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PayloadCoding.L12D
	# All values (3x):
	L12D | L34D | NONE

PayloadLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PayloadLength._255
	# All values (2x):
	_255 | _37

PduType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PduType.ADVDirect
	# All values (7x):
	ADVDirect | ADVind | ADVNonconn | ADVScan | CONReq | SCReq | SCRSp

PhyIsignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PhyIsignal.P4HP
	# All values (2x):
	P4HP | P8HP

Protocol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Protocol.CTSRts
	# All values (3x):
	CTSRts | NONE | XONXoff

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

Result
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Result.FAIL
	# All values (2x):
	FAIL | PASS

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RfConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RfConnector.I11I
	# Last value:
	value = enums.RfConnector.RH8
	# All values (163x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IFI1 | IFI2 | IFI3 | IFI4 | IFI5 | IFI6 | IQ1I | IQ3I
	IQ5I | IQ7I | R10D | R11 | R11C | R11D | R12 | R12C
	R12D | R12I | R13 | R13C | R14 | R14C | R14I | R15
	R16 | R17 | R18 | R21 | R21C | R22 | R22C | R22I
	R23 | R23C | R24 | R24C | R24I | R25 | R26 | R27
	R28 | R31 | R31C | R32 | R32C | R32I | R33 | R33C
	R34 | R34C | R34I | R35 | R36 | R37 | R38 | R41
	R41C | R42 | R42C | R42I | R43 | R43C | R44 | R44C
	R44I | R45 | R46 | R47 | R48 | RA1 | RA2 | RA3
	RA4 | RA5 | RA6 | RA7 | RA8 | RB1 | RB2 | RB3
	RB4 | RB5 | RB6 | RB7 | RB8 | RC1 | RC2 | RC3
	RC4 | RC5 | RC6 | RC7 | RC8 | RD1 | RD2 | RD3
	RD4 | RD5 | RD6 | RD7 | RD8 | RE1 | RE2 | RE3
	RE4 | RE5 | RE6 | RE7 | RE8 | RF1 | RF1C | RF2
	RF2C | RF2I | RF3 | RF3C | RF4 | RF4C | RF4I | RF5
	RF5C | RF6 | RF6C | RF7 | RF7C | RF8 | RF8C | RF9C
	RFAC | RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5
	RG6 | RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5
	RH6 | RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

RxQualityMeasMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RxQualityMeasMode.PER
	# All values (3x):
	PER | SENS | SPOT

SegmentPacketType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SegmentPacketType.ADVertiser
	# Last value:
	value = enums.SegmentPacketType.RFPHytest
	# All values (13x):
	ADVertiser | DATA | DH1 | DH3 | DH5 | E21P | E23P | E25P
	E31P | E33P | E35P | RFCTe | RFPHytest

StopBits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopBits.S1
	# All values (2x):
	S1 | S2

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.NONE
	# All values (2x):
	NONE | SLFail

SyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncState.ADJusted
	# All values (2x):
	ADJusted | PENDing

TargetMainState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetMainState.OFF
	# All values (3x):
	OFF | RDY | RUN

TestScenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestScenario.CSPath
	# All values (3x):
	CSPath | SALone | UNDefined

TransmitPatternType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TransmitPatternType.ALL1
	# All values (2x):
	ALL1 | OTHer

TxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnector.I12O
	# Last value:
	value = enums.TxConnector.RH18
	# All values (86x):
	I12O | I14O | I16O | I18O | I22O | I24O | I26O | I28O
	I32O | I34O | I36O | I38O | I42O | I44O | I46O | I48O
	IFO1 | IFO2 | IFO3 | IFO4 | IFO5 | IFO6 | IQ2O | IQ4O
	IQ6O | IQ8O | R10D | R118 | R1183 | R1184 | R11C | R11D
	R11O | R11O3 | R11O4 | R12C | R12D | R13C | R13O | R14C
	R214 | R218 | R21C | R21O | R22C | R23C | R23O | R24C
	R258 | R318 | R31C | R31O | R32C | R33C | R33O | R34C
	R418 | R41C | R41O | R42C | R43C | R43O | R44C | RA18
	RB14 | RB18 | RC18 | RD18 | RE18 | RF18 | RF1C | RF1O
	RF2C | RF3C | RF3O | RF4C | RF5C | RF6C | RF7C | RF8C
	RF9C | RFAC | RFAO | RFBC | RG18 | RH18

TxConnectorBench
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnectorBench.R118
	# Last value:
	value = enums.TxConnectorBench.RH18
	# All values (15x):
	R118 | R214 | R218 | R258 | R318 | R418 | RA18 | RB14
	RB18 | RC18 | RD18 | RE18 | RF18 | RG18 | RH18

TxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConverter.ITX1
	# Last value:
	value = enums.TxConverter.TX44
	# All values (40x):
	ITX1 | ITX11 | ITX12 | ITX13 | ITX14 | ITX2 | ITX21 | ITX22
	ITX23 | ITX24 | ITX3 | ITX31 | ITX32 | ITX33 | ITX34 | ITX4
	ITX41 | ITX42 | ITX43 | ITX44 | TX1 | TX11 | TX12 | TX13
	TX14 | TX2 | TX21 | TX22 | TX23 | TX24 | TX3 | TX31
	TX32 | TX33 | TX34 | TX4 | TX41 | TX42 | TX43 | TX44

