RepCaps
=========

Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst16
	# All values (16x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16

Segment
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Segment.S1
	# Range:
	S1 .. S128
	# All values (128x):
	S1 | S2 | S3 | S4 | S5 | S6 | S7 | S8
	S9 | S10 | S11 | S12 | S13 | S14 | S15 | S16
	S17 | S18 | S19 | S20 | S21 | S22 | S23 | S24
	S25 | S26 | S27 | S28 | S29 | S30 | S31 | S32
	S33 | S34 | S35 | S36 | S37 | S38 | S39 | S40
	S41 | S42 | S43 | S44 | S45 | S46 | S47 | S48
	S49 | S50 | S51 | S52 | S53 | S54 | S55 | S56
	S57 | S58 | S59 | S60 | S61 | S62 | S63 | S64
	S65 | S66 | S67 | S68 | S69 | S70 | S71 | S72
	S73 | S74 | S75 | S76 | S77 | S78 | S79 | S80
	S81 | S82 | S83 | S84 | S85 | S86 | S87 | S88
	S89 | S90 | S91 | S92 | S93 | S94 | S95 | S96
	S97 | S98 | S99 | S100 | S101 | S102 | S103 | S104
	S105 | S106 | S107 | S108 | S109 | S110 | S111 | S112
	S113 | S114 | S115 | S116 | S117 | S118 | S119 | S120
	S121 | S122 | S123 | S124 | S125 | S126 | S127 | S128

